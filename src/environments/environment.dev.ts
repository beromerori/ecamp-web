export const environment = {
  production: false,
  mode: 'Development',
  ip: 'https://ecamp-back.herokuapp.com/ecamp/api',
  //paypal: 'https://www.paypalobjects.com/api/checkout.js',
  //client_id: 'Abw2w9_U8f9_LL66JWPfNkNmJr667DGkPfJmmoy_vgtErOv2lkSnwcAbI9OLseyRPN2KXByNZbUt959t'
  paypal_env: 'sandbox',
  paypal_client_id: 'Abw2w9_U8f9_LL66JWPfNkNmJr667DGkPfJmmoy_vgtErOv2lkSnwcAbI9OLseyRPN2KXByNZbUt959t',
  paypal_script: 'https://www.paypal.com/sdk/js?client-id=Abw2w9_U8f9_LL66JWPfNkNmJr667DGkPfJmmoy_vgtErOv2lkSnwcAbI9OLseyRPN2KXByNZbUt959t&currency=EUR'
};