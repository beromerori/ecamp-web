import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

// CONST
import { CONST } from './constants';

// My components
import { EcampInfoMessageComponent } from '../shared/components/ecamp-info-message/ecamp-info-message.component';

// My models
import { Camp, Modality } from '../shared/models';

// My services
import { AlertService } from '../shared/components/alert/alert.service';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private alertService: AlertService,
    private snackBar: MatSnackBar) { }

  // Alert
  public openAlert(type: number, subtitle: string, title?: string) {

    if (!title) title = type === CONST.ALERT_TYPES.CONFIRM ? 'ALERT.NOTICE' : 'ALERT.OOPS';
    return this.alertService.createAlert(type, title, subtitle);
  }

  public openPaypalAlert(camp: Camp) {
    return this.alertService.createPaypalAlert(camp);
  }

  public openErrorAlert(error: { status: number, message: string }) {
    return this.alertService.presentErrorAlert(error);
  }

  // SnackBar
  public showInfoMsg(type: string, msg: string) {

    this.snackBar.openFromComponent(EcampInfoMessageComponent, {
      data: { type, msg },
      verticalPosition: 'top',
      duration: 3000,
      panelClass: [type]
    });
  }

  // Item without empty fields
  public checkFields(item: any) {

    for (let field in item) {
      if (!item[field]) delete item[field];
    }

    return item;
  }

  // Modalities

  public getModalities(checkedModalities: Modality[]) {

    let modalities = JSON.parse(JSON.stringify(CONST.MODALITIES));

    modalities = modalities.map(modality => {

      const index = checkedModalities.findIndex(m => m.id === modality.id);
      if (index > -1) modality.checked = true;
      return modality;

    });

    return modalities;
  }

  public getModalitiesIds(camp: Camp) {
    camp.modalities = camp.modalities.filter(modality => modality.checked);
    return camp.modalities.map(modality => modality.id);
  }

  public getCheckedModalities(modalitiesIds: number[]) {

    let modalities = JSON.parse(JSON.stringify(CONST.MODALITIES));

    modalities = modalities.map(modality => {

      const index = modalitiesIds.findIndex(modalityId => modalityId === modality.id);
      if (index > -1) modality.checked = true;

      return modality;
    })

    return modalities.filter(modality => modality.checked);
  }
}