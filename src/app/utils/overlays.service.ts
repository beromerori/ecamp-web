import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

// Alert
import { MatDialog } from '@angular/material';

// Rxjs
import { forkJoin, from, Observable, of, throwError } from 'rxjs';
import { catchError, delayWhen, mapTo, shareReplay, startWith, switchMap, delay } from 'rxjs/operators';

// Services
import { LoadingService } from './loading.service';
import { AlertDialogComponent } from '../shared/components';

@Injectable({
  providedIn: 'root'
})
export class OverlaysService {

  constructor(
    private dialog: MatDialog,
    private loadingService: LoadingService,
    private translateService: TranslateService) { }

  public requestWithLoaderAndError(request: () => Observable<any>) {

    const obs = of({}).pipe(
      this.presentLoader(),
      switchMap(() => request().pipe(this.onErrorDismissLoaderAndPresentError())),
      this.dismissLoader(),
      shareReplay()
    );

    obs.subscribe(
      (success) => { },
      (error) => { }
    );

    return obs;
  }

  public buildLoader(obs: Observable<any>) {

    return obs.pipe(
      delay(200),
      mapTo(false),
      startWith(true),
      this.onErrorDismissLoaderAndPresentError(),
      catchError((error) => of(false)),
      shareReplay()
    );
  }


  public onErrorDismissLoaderAndPresentError() {

    return catchError(error =>
      forkJoin(
        from(this.hideLoader()),
        from(this.presentErrorAlert(error))
      ).pipe(switchMap(() => throwError({ error: error.error.message })))
    );
  }

  // Loading

  public presentLoader() {
    return delayWhen(() => from(this.showLoader()));
  }

  private async showLoader() {
    await this.loadingService.showLoading();
  }

  public dismissLoader() {
    return delayWhen(() => from(this.hideLoader()));
  }

  private async hideLoader() {
    await this.loadingService.hideLoading();
  }

  // Alert

  public presentErrorAlert(error: { error: { status: number, message: string } }) {

    if (error.error.status === 502) return;
    //if (!error.error.status) error.error.message = 'SERVER.ERRORS.NOT-CONNECTION';

    return new Promise((resolve, reject) => {

      const alert = this.dialog.open(AlertDialogComponent, {
        data: {
          type: 3,
          title: this.translateService.instant('ALERT.OOPS'),
          subtitle: this.translateService.instant(error.error.message)
        }
      });

      alert.afterClosed().subscribe(
        (success) => resolve(),
        (error) => reject()
      );
    });
  }
}