import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { pluck } from 'rxjs/operators';

export interface State {
  loading: boolean
}

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  public initialState: State = {
    loading: false
  };

  private loadingSubject$ = new BehaviorSubject<State>(this.initialState);
  public loading$ = this.loadingSubject$.asObservable().pipe(pluck('loading'));

  constructor() { }

  get state() {
    return this.loadingSubject$.getValue();
  }

  get currentLoading() {
    return this.loadingSubject$.getValue().loading;
  }

  public showLoading() {

    return new Promise((resolve, reject) => {
      this.loadingSubject$.next({ loading: true });
      resolve();
    });
  }

  public hideLoading() {

    return new Promise((resolve, reject) => {
      this.loadingSubject$.next({ loading: false });
      resolve();
    });
  }
}