import { Component } from '@angular/core';

// My Services
import { LanguageService } from './shared/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private languageService: LanguageService
  ) {

    this.languageService.getLanguage();
    this.languageService.updateLanguage();
  }
}