import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Redux
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools'

// Reducers
import { languageReducer } from './language.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot({
      languageReducer
    }),
    StoreDevtoolsModule.instrument({})
  ],
  declarations: []
})
export class RootReducerModule { }