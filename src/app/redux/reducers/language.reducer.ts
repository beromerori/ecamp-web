// Redux
import { AppStore, initState } from "../appStore";

// States & Actions
import { LANGUAGE_STATES, LanguageActions } from '../actions/language.actions';

export type LanguageActions = LanguageActions;

export function languageReducer(state: AppStore = initState, action: LanguageActions) {

    switch (action.type) {

        case LANGUAGE_STATES.GET_LANGUAGE:
            return { language: action.payload };

        case LANGUAGE_STATES.SET_LANGUAGE:
            return { language: action.payload };

        default:
            return { language: state.language };
    }
}