// My Models
import { User, Camp } from "../shared/models";

export interface AppStore {
    language?: string
}

export const initState: AppStore = {
    language: ''
}