// Redux
import { Action } from "@ngrx/store";

// States
export const LANGUAGE_STATES = {
    GET_LANGUAGE: 'GET_LANGUAGE',
    SET_LANGUAGE: 'SET_LANGUAGE'
}

// Get language
export class GetLanguage implements Action {

    readonly type = LANGUAGE_STATES.GET_LANGUAGE;

    constructor(public payload: string) { }
}

// Set language
export class SetLanguage implements Action {

    readonly type = LANGUAGE_STATES.SET_LANGUAGE;

    constructor(public payload: string) { }
}

export type LanguageActions = GetLanguage | SetLanguage;