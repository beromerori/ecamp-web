import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Modal
import { Modal, overlayConfigFactory } from 'ngx-modialog';

// Rxjs
import { delay } from 'rxjs/operators';

// My components (barrel)
import { CampFiltersModalComponent } from '../../../shared/components/modals';

// My models
import { Camp, CampFilters } from '../../../shared/models';

// My services (barrel)
import { CampService } from '../../../shared/services';
import { OverlaysService } from '../../../utils/overlays.service';

@Component({
  selector: 'app-web-camps',
  templateUrl: './web-camps.component.html',
  styleUrls: ['./web-camps.component.scss']
})
export class WebCampsComponent implements OnInit {

  public camps$ = this.campService.camps$;
  public total$ = this.campService.total$;
  public loading$;

  public filters: CampFilters = { limit: 3, page: 1 };

  constructor(
    private modal: Modal,
    private router: Router,
    private campService: CampService,
    private overlaysService: OverlaysService) { }

  ngOnInit() {
    this.getCamps();
  }

  private getCamps(loadMore?: boolean, camps?: Camp[]) {

    if (loadMore) {
      this.filters.last_id = camps[camps.length - 1]._id;
      this.filters.page += 1;
    }

    this.loading$ = this.overlaysService.buildLoader(this.campService.getCamps(this.filters, loadMore).pipe(
      delay(200)
    ));
  }

  public showMore(camps: Camp[], total: number) {
    return camps.length < total;
  }

  public filterCamps() {

    const campFiltersModal = this.modal.open(CampFiltersModalComponent, overlayConfigFactory({
      dialogClass: 'ecamp-modal-lg ecamp-modal',
      filters: this.filters
    }));

    campFiltersModal.result.then((campFilters: CampFilters) => {

      if (!campFilters) return;

      this.filters = { ...campFilters, limit: this.filters.limit, page: 1 };
      this.getCamps();
    });
  }

  public goToCampDetail(camp: Camp) {
    this.router.navigate(['ecamp/web/camp-detail', camp._id]);
  }
}