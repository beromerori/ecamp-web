// Camp-detail
import { WebCampDetailComponent } from './web-camp-detail/web-camp-detail.component';
export { WebCampDetailComponent } from './web-camp-detail/web-camp-detail.component';

// Camps-list
import { WebCampsComponent } from './web-camps/web-camps.component';
export { WebCampsComponent } from './web-camps/web-camps.component';

// Create/Change-password
import { WebPasswordComponent } from './web-password/web-password.component';
export { WebPasswordComponent } from './web-password/web-password.component';

export const WebPages: any[] = [
    WebCampDetailComponent,
    WebCampsComponent,
    WebPasswordComponent
];