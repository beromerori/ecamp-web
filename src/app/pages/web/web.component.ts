import { Component, OnInit } from '@angular/core';

// My services
import { LanguageService } from '../../shared/services';

@Component({
  selector: 'app-web',
  templateUrl: './web.component.html',
  styleUrls: ['./web.component.scss']
})
export class WebComponent implements OnInit {

  constructor(private languageService: LanguageService) { }

  ngOnInit() {
    this.languageService.getLanguage();
    this.languageService.updateLanguage();
  }
}