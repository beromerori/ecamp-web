import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Routing
import { WebRouting } from './web-routing.module';
import { WebComponent } from './web.component';

// My modules
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '../../shared/material.module';

// My pages
import { WebPages } from './index';

// My services
import { Services } from '../../shared/services';
//import { FormatDatePipe } from 'src/app/shared/pipes';
//import { SmallCampComponent } from 'src/app/shared/components';

@NgModule({
  declarations: [
    WebComponent,
    ...WebPages
  ],
  imports: [
    CommonModule,
    WebRouting,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      isolate: true
    }),
    SharedModule,
    MaterialModule
  ],
  providers: [
    ...Services
  ]
})
export class WebModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}