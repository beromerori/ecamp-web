import { Routes, RouterModule } from '@angular/router';

import { WebComponent } from './web.component';

// My pages
import { WebCampDetailComponent } from './web-camp-detail/web-camp-detail.component';
import { WebCampsComponent } from './web-camps/web-camps.component';
import { WebPasswordComponent } from './web-password/web-password.component';

const webRoutes: Routes = [
  {
    path: '',
    component: WebComponent,
    children: [
      // Landing
      // Camps-list
      { path: 'camps', component: WebCampsComponent },
      // Camp-detail
      { path: 'camp-detail/:id', component: WebCampDetailComponent },
      // Create/change password
      { path: 'password', component: WebPasswordComponent }
    ]
  }
];

export const WebRouting = RouterModule.forChild(webRoutes);