import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

// Redux
import { Store } from '@ngrx/store';
import { AppStore } from '../../../redux/appStore';
import * as LanguageActions from '../../../redux/actions/language.actions';

// Rxjs
import { delay, tap } from 'rxjs/operators';

// CONST
import { CONST } from '../../../utils/constants';

// Services (barrel)
import { UserService } from '../../../shared/services';
import { OverlaysService } from '../../../utils/overlays.service';
import { UtilsService } from '../../../utils/utils.service';

@Component({
  selector: 'app-web-password',
  templateUrl: './web-password.component.html',
  styleUrls: ['./web-password.component.scss']
})
export class WebPasswordComponent implements OnInit {

  public passwordForm: FormGroup;
  public passwordsInvalid: boolean = false;

  public iconVisivilitySelector = { 1: 'fas fa-eye', 2: 'fas fa-eye' };
  private hideSelector = { 1: true, 2: true };

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<AppStore>,
    private overlaysService: OverlaysService,
    private userService: UserService,
    private utilsService: UtilsService) { }

  ngOnInit() {

    this.activatedRoute.queryParams.subscribe(
      (queryParams: { email: string, language: string }) => {

        if (!queryParams) queryParams = { email: '', language: localStorage.getItem('lang') };
        this.store.dispatch(new LanguageActions.SetLanguage(queryParams.language));
        this.initForm(queryParams);
      }
    );
  }

  private initForm(queryParams: { email: string }) {

    this.passwordForm = new FormGroup({
      email: new FormControl({ value: queryParams.email, disabled: queryParams.email }, [Validators.required, Validators.pattern(CONST.VALIDATIONS.EMAIL_REGEX)]),
      newPassword: new FormControl('', [Validators.required, Validators.pattern(CONST.VALIDATIONS.PASS_REGEX)]),
      repeatPassword: new FormControl('', [Validators.required, Validators.pattern(CONST.VALIDATIONS.PASS_REGEX)])
    });

    this.repeatPassword.valueChanges.subscribe(
      (pass: string) => this.passwordsInvalid = pass && (pass !== this.newPassword.value)
    );
  }

  get email() { return this.passwordForm.get('email'); }
  get newPassword() { return this.passwordForm.get('newPassword'); }
  get repeatPassword() { return this.passwordForm.get('repeatPassword'); }

  public getPasswordType(index: number) {
    return this.hideSelector[index] ? 'password' : 'text';
  }

  public changeVisibilityPassword(index: number) {
    this.hideSelector[index] = !this.hideSelector[index];
    this.iconVisivilitySelector[index] = this.hideSelector[index] ? 'fas fa-eye' : 'fas fa-eye-slash';
  }

  public send() {

    if (this.passwordForm.invalid || this.passwordsInvalid) return;

    this.overlaysService.requestWithLoaderAndError(() => this.userService.updateUserPassword({ email: this.email.value, pass: this.newPassword.value }).pipe(
      delay(200),
      tap(() => {

        const alert = this.utilsService.openAlert(CONST.ALERT_TYPES.INFO, 'ALERT.PASSWORD-CHANGED', 'ALERT.CONGRATULATIONS');

        alert.afterClosed().subscribe(
          () => this.router.navigate(['web/camps'])
        );
      })
    ));
  }
}