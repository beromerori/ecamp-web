import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

// Rxjs
import { delay, pluck, tap } from 'rxjs/operators';

// Modal
import { Modal, overlayConfigFactory } from 'ngx-modialog';

// My components (barrel)
import { ApplicationModalComponent } from '../../../shared/components/modals';

// My models
import { Camp, Child, Modality, Parent } from '../../../shared/models';

// My services (barrel)
import { CampService } from '../../../shared/services';
import { UtilsService } from '../../../utils/utils.service';
import { OverlaysService } from '../../../utils/overlays.service';

@Component({
  selector: 'app-web-camp-detail',
  templateUrl: './web-camp-detail.component.html',
  styleUrls: ['./web-camp-detail.component.scss']
})
export class WebCampDetailComponent implements OnInit {

  public camp$;
  public checkedModalites: Modality[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private modal: Modal,
    private campService: CampService,
    private overlaysService: OverlaysService,
    private translateService: TranslateService,
    private utilsService: UtilsService) { }

  ngOnInit() {

    this.activatedRoute.params.pipe(pluck('id')).subscribe(
      (id: string) => this.getCamp(id)
    );
  }

  private getCamp(id: string) {

    this.overlaysService.requestWithLoaderAndError(() => this.campService.getCamp(id).pipe(
      delay(200)
    ));

    this.camp$ = this.campService.selectById(id);
  }

  public openPDF(camp: Camp) {
    window.open(camp.pdf.secure_url || camp.pdf.url);
  }

  public openInscriptionModal(camp: Camp) {

    const applicationModal = this.modal.open(ApplicationModalComponent, overlayConfigFactory(
      {
        dialogClass: 'ecamp-modal-lg ecamp-modal',
        camp
      }
    ));

    applicationModal.result.then(
      (data: { child: Child, parent: Parent }) => {

        if (!data) return;
        this.openPaypalAlert(data.child, data.parent, camp);
      }
    );
  }

  private openPaypalAlert(child, parent, camp) {

    const paypalAlert = this.utilsService.openPaypalAlert(camp);

    paypalAlert.afterClosed().subscribe(
      (payment) => {

        if (payment.id) {
          this.createApplication(child, parent, camp);
          this.createBill(parent, camp);
        }
      }
    );
  }

  private createApplication(child: Child, parent: Parent, camp: Camp) {

    this.overlaysService.requestWithLoaderAndError(() => this.campService.createApplication(child, parent, camp).pipe(
      delay(200),
      tap(() => this.utilsService.openAlert(2, this.translateService.instant('ALERT.PAYPAL.ACCEPTED-PAYMENT'), this.translateService.instant('ALERT.PAYPAL.CONGRATULATIONS')))
    ));
  }

  private createBill(parent: Parent, camp: Camp) {

    this.overlaysService.requestWithLoaderAndError(() => this.campService.createBill(parent, camp).pipe(
      delay(200)
    ));
  }
}