import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

// Moment
import * as moment from 'moment';

// Rxjs
import { delay, tap } from 'rxjs/operators';

// My models
import { Camp, MediaFile, Programming } from '../../../shared/models';

// My services (barrel)
import { CampService, MediaService, ProgrammingService } from '../../../shared/services';
import { OverlaysService } from '../../../utils/overlays.service';
import { UtilsService } from '../../../utils/utils.service';

@Component({
  selector: 'app-crm-programmings',
  templateUrl: './crm-programmings.component.html',
  styleUrls: ['./crm-programmings.component.scss']
})
export class CrmProgrammingsComponent implements OnInit {

  public programmingForm: FormGroup;

  public programmings$ = this.programmingService.programmings$;
  public programming$;

  public code: string;
  public minDate;
  public maxDate;

  public images: MediaFile[] = [];

  constructor(
    private campService: CampService,
    private mediaService: MediaService,
    private overlaysService: OverlaysService,
    private programmingService: ProgrammingService,
    private utilsService: UtilsService) { }

  ngOnInit() {
    this.initForm({ date: {} });
    this.getCamp();
    this.getProgrammings();
  }

  private initForm(programming: Programming) {

    this.programmingForm = new FormGroup({
      _id: new FormControl(programming._id),
      title: new FormControl(programming.title, Validators.required),
      date: new FormControl(moment(programming.date.timestamp)),
      body: new FormControl(programming.body, Validators.required),
      camp_id: new FormControl(sessionStorage.getItem('camp_id'), Validators.required),
    });

    this.programming$ = this.programmingService.selectById(programming._id);
  }

  get title() { return this.programmingForm.get('title'); }
  get date() { return this.programmingForm.get('date'); }
  get body() { return this.programmingForm.get('body'); }
  get camp_id() { return this.programmingForm.get('camp_id'); }

  private getCamp() {

    const camp_id = sessionStorage.getItem('camp_id');

    this.overlaysService.requestWithLoaderAndError(() => this.campService.getCamp(camp_id).pipe(
      delay(200),
      tap((camp: Camp) => {
        this.code = camp.code;
        this.date.patchValue(moment(camp.date.start.timestamp));
        this.minDate = moment(camp.date.start.timestamp);
        this.maxDate = moment(camp.date.end.timestamp);
      })
    ));
  }

  private getProgrammings() {

    this.overlaysService.requestWithLoaderAndError(() => this.programmingService.getProgrammings(this.camp_id.value).pipe(
      delay(200)
    ));
  }

  public newProgramming() {
    this.programmingService.currentProgrammings.forEach(p => p.selected = false);
    this.images = [];
    this.initForm({ date: {} });
  }

  public selectProgramming(programming: Programming) {

    this.programmingService.currentProgrammings.forEach(p => p.selected = false);
    programming.selected = true;
    this.images = programming.images;

    this.initForm(programming);
  }

  public async saveProgramming() {

    if (this.programmingForm.invalid) return;

    const uploadedImages = await this.uploadImages();
    if (uploadedImages) this.images = uploadedImages;

    let programming: Programming = { ...this.programmingForm.value, images: this.images, date: { time: moment(this.date.value).locale('es').format('L'), timestamp: moment(this.date.value).valueOf() } };
    programming = this.utilsService.checkFields(programming);

    if (!programming._id) this.createProgramming(programming);
    else this.updateProgramming(programming);
  }

  private createProgramming(programming: Programming) {

    this.overlaysService.requestWithLoaderAndError(() => this.programmingService.createProgramming(programming).pipe(
      delay(200),
      tap((newProgramming: Programming) => {

        this.initForm(newProgramming);
        this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.PROGRAMMING.CREATED');
      })
    ));
  }

  private updateProgramming(programming: Programming) {

    this.overlaysService.requestWithLoaderAndError(() => this.programmingService.updateProgramming(programming).pipe(
      delay(200),
      tap(() => this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.PROGRAMMING.UPDATED'))
    ));
  }

  public removeProgramming(programming: Programming) {

    const confirmAlert = this.utilsService.openAlert(1, 'ALERT.REMOVE-PROGRAMMING-ANSWER');

    confirmAlert.afterClosed().subscribe((deleted: boolean) => {
      if (deleted) this.deleteProgramming(programming);
    });
  }

  private deleteProgramming(programming: Programming) {

    this.overlaysService.requestWithLoaderAndError(() => this.programmingService.deleteProgramming(programming).pipe(
      delay(200),
      tap(() => {
        this.initForm({ date: {} });
        this.images = [];
        this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.PROGRAMMING.DELETED');
      })
    ));
  }

  // Images

  private uploadImages(): Promise<MediaFile[]> {

    return new Promise(async (resolve, reject) => {

      const images = this.images.map((image: MediaFile) => {

        if (!image.secure_url) {
          return this.mediaService.uploadFile('camp', this.code, image.url);
        }
        return image;
      });

      await Promise.all(images)
        .then((images: MediaFile[]) => resolve(images))
        .catch((error) => {
          this.utilsService.openErrorAlert(error);
          reject();
        });
    });
  }

  public updateImages(images: MediaFile[]) {
    this.images = images;
  }
}