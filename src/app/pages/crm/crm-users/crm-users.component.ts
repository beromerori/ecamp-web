import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// Modal
import { Modal, overlayConfigFactory } from 'ngx-modialog';

// Rxjs
import { Subscription } from 'rxjs';
import { delay, tap } from 'rxjs/operators';

// CONST
import { CONST } from '../../../utils/constants';

// My Components (barrel)
import { UserModalComponent } from '../../../shared/components/modals';

// My Models
import { MediaFile, User, UserFilters } from '../../../shared/models';

// My Services (barrel)
import { MediaService, UserService } from '../../../shared/services';
import { OverlaysService } from '../../../utils/overlays.service';
import { UtilsService } from '../../../utils/utils.service';

@Component({
  selector: 'app-crm-users',
  templateUrl: './crm-users.component.html',
  styleUrls: ['./crm-users.component.scss']
})
export class CrmUsersComponent implements OnInit, OnDestroy {

  public role: number = 1;
  public title: string = '';

  private queryParams$: Subscription = new Subscription();

  public users$;
  public loading$;
  public userFilters: UserFilters = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    private modal: Modal,
    private mediaService: MediaService,
    private overlaysService: OverlaysService,
    private userService: UserService,
    private utilsService: UtilsService) { }

  ngOnInit() {

    this.queryParams$ = this.activatedRoute.queryParams.subscribe(
      (queryParams: { role: string }) => {

        this.userFilters.role = Number(queryParams.role);
        this.title = this.userFilters.role === CONST.ROLES.PARENT ? 'USERS.TITLE-PARENTS' : 'USERS.TITLE-INSTRUCTORS';

        this.getUsers();
      }
    );
  }

  ngOnDestroy() {
    this.queryParams$.unsubscribe();
  }

  private getUsers() {

    this.loading$ = this.overlaysService.buildLoader(this.userService.getUsers(this.userFilters).pipe(
      delay(200)
    ));

    this.users$ = this.userService.selectByRole(this.userFilters.role);
  }

  public isInstructor() {
    return this.userFilters.role === CONST.ROLES.INSTRUCTOR;
  }

  public newUser() {

    const newUserModal = this.modal.open(UserModalComponent, overlayConfigFactory({
      dialogClass: 'ecamp-modal-md ecamp-modal',
      user: { role: 2, camp: { campType: 1 } }
    }));

    newUserModal.result.then(async (data) => {

      if (!data) return;

      const user = this.utilsService.checkFields(data);

      if (user.cv) {

        await this.mediaService.uploadFile('user', user.email, user.cv)
          .then((cv: MediaFile) => {
            if (cv) user.cv = cv;
          })
          .catch((error) => this.utilsService.openErrorAlert(error));
      }

      this.createUser(user);
    });
  }

  public async editUser(data: { updatedUser: any, oldCV: any }) {

    const user = this.utilsService.checkFields(data.updatedUser);

    if (user.cv) {

      await this.mediaService.uploadFile('user', user.email, user.cv)
        .then((cv: MediaFile) => {
          if (cv) user.cv = cv;
        })
        .catch((error) => this.utilsService.openErrorAlert(error));
    }
    else user.cv = data.oldCV;

    this.updateUser(user);
  }

  private createUser(user: User) {

    this.overlaysService.requestWithLoaderAndError(() => this.userService.createUser(user).pipe(
      delay(200),
      tap(() => this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.USER.CREATED'))
    ));

    this.users$ = this.userService.selectByRole(this.userFilters.role);
  }

  private updateUser(user: User) {
    this.overlaysService.requestWithLoaderAndError(() => this.userService.updateUser(user).pipe(
      delay(200),
      tap(() => this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.USER.UPDATED'))
    ));
  }

  public deleteUser(user) {

    this.overlaysService.requestWithLoaderAndError(() => this.userService.deleteUser(user._id).pipe(
      delay(200),
      tap(() => {

        if (user.childs_ids.length) {
          user.childs_ids.forEach(child_id => this.userService.deleteUser(child_id));
        }

        this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.USER.DELETED')
      })
    ));
  }
}