import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// Modal
import { Modal, overlayConfigFactory } from 'ngx-modialog';

// Rxjs
import { delay, tap } from 'rxjs/operators';

// CONST
import { CONST } from '../../../utils/constants';

// My components (barrel)
import { CampModalComponent } from '../../../shared/components/modals';

// My models
import { Camp } from '../../../shared/models';

// My Services (barrel)
import { CampService, MediaService } from '../../../shared/services';
import { OverlaysService } from '../../../utils/overlays.service';
import { UtilsService } from '../../../utils/utils.service';

@Component({
  selector: 'app-crm-camps',
  templateUrl: './crm-camps.component.html',
  styleUrls: ['./crm-camps.component.scss']
})
export class CrmCampsComponent implements OnInit {

  private type: number;
  public title: string;

  public camps$;
  public loading$;

  constructor(
    private activatedRoute: ActivatedRoute,
    private modal: Modal,
    private campService: CampService,
    private mediaService: MediaService,
    private overlaysService: OverlaysService,
    private utilsService: UtilsService) { }

  ngOnInit() {

    this.activatedRoute.queryParams.subscribe(
      (queryParams) => {
        this.type = Number(queryParams.type);
        this.title = this.type === CONST.CAMP_TYPES.AUTONOMOUS ? 'CAMPS.TITLE-AUTONOMOUS' : 'CAMPS.TITLE-EXCHANGES';
        this.getCamps();
      }
    );
  }

  private getCamps() {

    this.loading$ = this.overlaysService.buildLoader(this.campService.getCamps({ type: this.type }).pipe(
      delay(200)
    ));
    this.camps$ = this.campService.selectByType(this.type);
  }

  public newCamp() {

    const newCampModal = this.modal.open(CampModalComponent, overlayConfigFactory({
      dialogClass: 'ecamp-modal-lg ecamp-modal',
      camp: {
        date: { start: {}, end: {} },
        age: {},
        numApplications: 0,
        ccaa: 0,
        images: [],
        modalities: JSON.parse(JSON.stringify(CONST.MODALITIES))
      }
    }));

    newCampModal.result.then((newCamp: Camp) => {
      if (!newCamp) return;
      this.createOrUpdateCamp(newCamp);
    });
  }

  private async createOrUpdateCamp(camp: Camp) {

    let pdf;

    camp.images = camp.images.map((image: any) => {

      if (!image.secure_url) {
        return this.mediaService.uploadFile('camp', camp.code, image.url);
      }
      return image;
    });

    await Promise.all(camp.images)
      .then((images) => {
        camp.images = images;
      })
      .catch((error) => this.utilsService.openErrorAlert(error));

    if (camp.pdf && !camp.pdf.secure_url) {

      await this.mediaService.uploadFile('camp', camp.code, camp.pdf.url)
        .then((pdf) => camp.pdf = pdf)
        .catch((error) => this.utilsService.openErrorAlert(error));
    }

    if (!camp._id) this.createCamp(camp);
    else this.updateCamp(camp);
  }

  private createCamp(camp: Camp) {

    this.overlaysService.requestWithLoaderAndError(() => this.campService.createCamp(camp).pipe(
      delay(200),
      tap(() => this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.CAMP.CREATED'))
    ));
  }

  private updateCamp(camp: Camp) {

    this.overlaysService.requestWithLoaderAndError(() => this.campService.updateCamp(camp).pipe(
      delay(200),
      tap(() => this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.CAMP.UPDATED'))
    ));
  }

  public async deleteCamp(camp: Camp) {

    if (camp.images.length) {
      await this.mediaService.deleteAllFiles('camp', camp.code)
        .then((success) => { })
        .catch((error) => this.utilsService.openErrorAlert(error));
    }

    this.overlaysService.requestWithLoaderAndError(() => this.campService.deleteCamp(camp).pipe(
      delay(200),
      tap(() => this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.CAMP.DELETED'))
    ));
  }
}