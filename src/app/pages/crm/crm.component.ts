import { Component, OnInit } from '@angular/core';

// My services
import { LanguageService } from '../../shared/services';

@Component({
  selector: 'app-crm',
  templateUrl: './crm.component.html',
  styleUrls: ['./crm.component.scss']
})
export class CrmComponent implements OnInit {

  constructor(private languageService: LanguageService) { }

  ngOnInit() {
    this.languageService.getLanguage();
    this.languageService.updateLanguage();
  }
}