import { Component, OnInit } from '@angular/core';

// Rxjs
import { delay } from 'rxjs/operators';

// Services
import { OverlaysService } from '../../../utils/overlays.service';
import { UserService } from '../../../shared/services/user/user.service';

@Component({
  selector: 'app-crm-participants',
  templateUrl: './crm-participants.component.html',
  styleUrls: ['./crm-participants.component.scss']
})
export class CrmParticipantsComponent implements OnInit {

  public participants$;
  public loading$;

  constructor(
    private overlaysService: OverlaysService,
    private userService: UserService) { }

  ngOnInit() {
    this.getParticipants();
  }

  private getParticipants() {

    const camp_id = sessionStorage.getItem('camp_id');

    this.loading$ = this.overlaysService.buildLoader(this.userService.getParticipants(camp_id).pipe(
      delay(200)
    ));

    this.participants$ = this.userService.selectByCamp(camp_id);
  }
}