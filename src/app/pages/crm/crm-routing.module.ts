import { Routes, RouterModule } from '@angular/router';

import { CrmComponent } from './crm.component';

// My pages
import { CrmBillsComponent } from './crm-bills/crm-bills.component';
import { CrmCampsComponent } from './crm-camps/crm-camps.component';
import { CrmChartsComponent } from './crm-charts/crm-charts.component';
import { CrmParticipantsComponent } from './crm-participants/crm-participants.component';
import { CrmProgrammingsComponent } from './crm-programmings/crm-programmings.component';
import { CrmUsersComponent } from './crm-users/crm-users.component';

const crmRoutes: Routes = [
  {
    path: '',
    component: CrmComponent,
    children: [
      { path: 'bills', component: CrmBillsComponent },
      { path: 'camps', component: CrmCampsComponent },
      { path: 'charts', component: CrmChartsComponent },
      { path: 'users', component: CrmUsersComponent },
      { path: 'participants', component: CrmParticipantsComponent },
      { path: 'programmings', component: CrmProgrammingsComponent },
      { path: 'users', component: CrmUsersComponent }
    ]
  }
];

export const CrmRouting = RouterModule.forChild(crmRoutes);