import { Component } from '@angular/core';
import { Router } from '@angular/router';

// Rxjs
import { delay, tap } from 'rxjs/operators';

// CONSTANTS
import { CONST } from '../../../utils/constants';

// My Models
import { UserCredentials, UserLogged } from '../../../shared/models';

// My Services (barrel)
import { AuthService, ProfileService } from '../../../shared/services';
import { OverlaysService } from '../../../utils/overlays.service';

@Component({
  selector: 'app-crm-login',
  templateUrl: './crm-login.component.html',
  styleUrls: ['./crm-login.component.scss']
})
export class CrmLoginComponent {

  constructor(
    private router: Router,
    private authService: AuthService,
    private overlaysService: OverlaysService,
    private profileService: ProfileService) { }

  public login(userCredentials: UserCredentials) {

    this.overlaysService.requestWithLoaderAndError(() => this.authService.login(userCredentials).pipe(
      delay(200),
      tap((userLogged: UserLogged) => {
        sessionStorage.setItem('token', userLogged.token);
        sessionStorage.setItem('email', userCredentials.email);
        this.getProfile(userCredentials.email);
      })
    ));
  }

  private getProfile(email: string) {

    this.overlaysService.requestWithLoaderAndError(() => this.profileService.getProfile(email).pipe(
      delay(200),
      tap((user) => {

        if (user.camp && user.camp.camp_id) sessionStorage.setItem('camp_id', user.camp.camp_id);

        if (user.role === CONST.ROLES.ADMIN) this.router.navigate(['ecamp/crm/camps'], { queryParams: { type: CONST.CAMP_TYPES.AUTONOMOUS } });
        if (user.role === CONST.ROLES.INSTRUCTOR) this.router.navigate(['ecamp/crm/participants']);
      })
    ));
  }

  public goTochangePassword() {
    this.router.navigate(['ecamp/web/password']);
  }
}