import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClient } from '@angular/common/http';

// Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Routing
import { CrmRouting } from './crm-routing.module';
import { CrmComponent } from './crm.component';

// My modules
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '../../shared/material.module';

// My pages
import { CrmPages } from './index';

// My services
import { Services } from '../../shared/services';
import { FormatDatePipe } from 'src/app/shared/pipes';
import { SmallCampComponent } from 'src/app/shared/components';

@NgModule({
  declarations: [
    CrmComponent,
    ...CrmPages
  ],
  imports: [
    CommonModule,
    CrmRouting,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      isolate: true
    }),
    SharedModule,
    MaterialModule
  ],
  providers: [
    ...Services
  ]
})
export class CrmModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}