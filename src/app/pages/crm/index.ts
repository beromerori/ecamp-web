// Bills
import { CrmBillsComponent } from './crm-bills/crm-bills.component';
export { CrmBillsComponent } from './crm-bills/crm-bills.component';

// Camps
import { CrmCampsComponent } from './crm-camps/crm-camps.component';
export { CrmCampsComponent } from './crm-camps/crm-camps.component';

// Charts
import { CrmChartsComponent } from './crm-charts/crm-charts.component';
export { CrmChartsComponent } from './crm-charts/crm-charts.component';

// Participants
import { CrmParticipantsComponent } from './crm-participants/crm-participants.component';
export { CrmParticipantsComponent } from './crm-participants/crm-participants.component';

// Programmings
import { CrmProgrammingsComponent } from './crm-programmings/crm-programmings.component';
export { CrmProgrammingsComponent } from './crm-programmings/crm-programmings.component';

// Users
import { CrmUsersComponent } from './crm-users/crm-users.component';
export { CrmUsersComponent } from './crm-users/crm-users.component';

export const CrmPages: any[] = [
    CrmBillsComponent,
    CrmCampsComponent,
    CrmChartsComponent,
    CrmParticipantsComponent,
    CrmProgrammingsComponent,
    CrmUsersComponent
];