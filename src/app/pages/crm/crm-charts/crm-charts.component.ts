import { Component, OnInit } from '@angular/core';

// Rxjs
import { delay, tap } from 'rxjs/operators';

// Redux
import { Store } from '@ngrx/store';
import { AppStore } from '../../../redux/appStore';

// Services
import { ChartService } from '../../../shared/services/chart/chart.service';
import { OverlaysService } from '../../../utils/overlays.service';

@Component({
  selector: 'app-crm-charts',
  templateUrl: './crm-charts.component.html',
  styleUrls: ['./crm-charts.component.scss']
})
export class CrmChartsComponent implements OnInit {

  public charts$;
  public language$;

  constructor(
    private store: Store<AppStore>,
    private chartService: ChartService,
    private overlaysService: OverlaysService) { }

  ngOnInit() {
    this.getCharts();
    this.checkLanguage();
  }

  private getCharts() {
    this.charts$ = this.overlaysService.requestWithLoaderAndError(() => this.chartService.getCharts().pipe(
      delay(200)
    ));
  }

  private checkLanguage() {

    this.language$ = this.store.pipe(
      tap((languageReducer: any) => {
        if (languageReducer.languageReducer && languageReducer.languageReducer.language) return languageReducer.languageReducer.language;
      })
    );
  }
}