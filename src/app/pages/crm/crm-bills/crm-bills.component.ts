import { Component, OnInit } from '@angular/core';

// Rxjs
import { delay } from 'rxjs/operators';

// My services (barrel)
import { BillService } from '../../../shared/services';
import { OverlaysService } from '../../../utils/overlays.service';

@Component({
  selector: 'app-crm-bills',
  templateUrl: './crm-bills.component.html',
  styleUrls: ['./crm-bills.component.scss']
})
export class CrmBillsComponent implements OnInit {

  public bills$ = this.billService.bills$;
  public loading$;

  constructor(
    private billService: BillService,
    private overlaysService: OverlaysService) { }

  ngOnInit() {
    this.getBills();
  }

  private getBills() {

    this.loading$ = this.overlaysService.buildLoader(this.billService.getBills().pipe(
      delay(200)
    ));
  }
}