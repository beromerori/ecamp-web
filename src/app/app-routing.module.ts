import { Routes, RouterModule } from '@angular/router';

// Guards
import { UserLoggedGuard } from './shared/services/guards';

// CRM routes
import { CrmLoginComponent } from './pages/crm/crm-login/crm-login.component';

const routes: Routes = [

    // WEB
    { path: 'ecamp/web', loadChildren: './pages/web/web.module#WebModule' },

    // CRM
    { path: 'ecamp/crm', loadChildren: './pages/crm/crm.module#CrmModule', canActivate: [UserLoggedGuard] },
    { path: 'ecamp/crm/login', component: CrmLoginComponent },

    // Default
    { path: '', redirectTo: 'ecamp/web/camps', pathMatch: 'full' },
    { path: '**', redirectTo: 'ecamp/web/camps', pathMatch: 'full' }
];

export const AppRouting = RouterModule.forRoot(routes, {
    useHash: true
});