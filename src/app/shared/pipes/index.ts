import { FormatDatePipe } from './format-date.pipe';
export { FormatDatePipe } from './format-date.pipe';

export const Pipes: any[] = [FormatDatePipe];