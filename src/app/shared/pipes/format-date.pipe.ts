import { Pipe, PipeTransform } from '@angular/core';

// moment
import * as moment from 'moment';

@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {

  constructor() {
    moment.locale('es');
  }

  transform(value: any, args?: any): any {
    return moment(value).format('l');
  }
}