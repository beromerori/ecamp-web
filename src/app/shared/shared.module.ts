import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Charts
import { ChartsModule } from 'ng2-charts';

// Gallery
import { GalleryModule, GalleryConfig } from '@ngx-gallery/core';

// Modal
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';

// Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Angular material
import { MaterialModule } from './material.module';

// My components
import { Components } from './components';
//import { EcampLoadingComponent } from './components/ecamp-loading/ecamp-loading.component';
import { Modals } from './components/modals';

// My pipes
import { Pipes } from './pipes';

const config: GalleryConfig = {
  counter: true,
  nav: true,
  thumb: false
}

@NgModule({
  declarations: [
    // Components
    ...Components,
    // Pipes
    ...Pipes
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // Translate
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      isolate: true
    }),
    // Modal
    ModalModule.forRoot(),
    BootstrapModalModule,
    GalleryModule.forRoot(config),
    ChartsModule,
    MaterialModule
  ],
  exports: [...Components],
  entryComponents: [...Modals],
  providers: []
})
export class SharedModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}