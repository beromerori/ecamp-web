import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { pluck } from 'rxjs/operators';

// My models
import { Message } from '../../models';

// My services
import { BaseService } from '../base-service/base.service';
import { BaseEntityService } from '../../../utils/base-entity.service';

export interface State {
  messages?: Message[];
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  public initialState = {
    messages: []
  }

  private messagesSubject$ = new BehaviorSubject<State>(this.initialState);
  public messages$ = this.messagesSubject$.asObservable().pipe(pluck('messages'));

  constructor(
    private baseEntityService: BaseEntityService,
    private baseService: BaseService) { }

  get state() {
    return this.messagesSubject$.getValue();
  }

  get currentMessages() {
    return this.messagesSubject$.getValue().messages;
  }

  // GET
  public getMessages(parent_id: string) {

    const request = this.baseService.get('messages', { parent_id });

    request.subscribe(
      (messages: Message[]) => this.messagesSubject$.next({ messages: this.baseEntityService.createList('message', messages) }),
      (error) => { }
    );

    return request;
  }

  // POST
  public sendMessage(message: Message) {

    const request = this.baseService.post('messages', message);

    request.subscribe(
      (messages: Message) => this.messagesSubject$.next({ messages: this.baseEntityService.updateEntity('message', [...this.currentMessages], message) }),
      (error) => { }
    );

    return request;
  }
}