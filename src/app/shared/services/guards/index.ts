import { UserLoggedGuard } from './user-logged.guard';
export { UserLoggedGuard } from './user-logged.guard';

export const Guards: any[] = [UserLoggedGuard];