import { Injectable } from '@angular/core';

// My services
import { BaseService } from '../base-service/base.service';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  constructor(private baseService: BaseService) { }

  public getCharts() {
    return this.baseService.get('charts');
  }
}