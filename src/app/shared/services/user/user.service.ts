import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { filter, map, pluck } from 'rxjs/operators';

// My models
import { Child, Parent, User, UserFilters } from '../../models';

// My services
import { BaseEntityService } from '../../../utils/base-entity.service';
import { BaseService } from '../base-service/base.service';
import { MediaService } from '../media/media.service';

export interface State {
  users?: User[]
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public initialState = {
    users: []
  }

  private usersSubject$ = new BehaviorSubject<State>(this.initialState);
  public users$ = this.usersSubject$.asObservable().pipe(pluck('users'));

  constructor(
    private baseEntityService: BaseEntityService,
    private baseService: BaseService,
    private mediaService: MediaService) { }

  get state() {
    return this.usersSubject$.getValue();
  }

  get currentUsers() {
    return this.usersSubject$.getValue().users;
  }

  private userTypes = {
    1: () => 'parent',
    2: () => 'instructor',
    3: () => 'admin',
    null: () => 'child'
  };

  // GET: All users by filters & pagination
  public getUsers(filters?: UserFilters) {

    const userTypesHandler = this.userTypes[filters.role];
    const type = userTypesHandler();

    const request = this.baseService.get('users', filters);

    request.subscribe(
      (users) => this.usersSubject$.next({ users: this.baseEntityService.updateList(type, [...this.currentUsers], users) }),
      (error) => this.usersSubject$.next({ users: [] })
    );

    return request;
  }

  public getChilds(parent: Parent) {

    const request = this.baseService.get(`users/${parent._id}/childs`);

    request.subscribe(
      (childs: Child[]) => this.usersSubject$.next({ users: this.baseEntityService.updateList('child', [...this.currentUsers], childs) }),
      (error) => { }
    );

    return request;
  }

  public getParticipants(camp_id: string) {

    const request = this.baseService.get('users/participants', { camp_id });

    request.subscribe(
      (users: User[]) => {
        users = users.filter(u => !u.role);
        this.usersSubject$.next({ users: this.baseEntityService.createList('child', users) });
      },
      (error) => this.usersSubject$.next({ users: [] })
    );

    return request;
  }

  // GET: User by id
  public getUser(id: string) {

    const request = this.baseService.get(`users/${id}`);

    request.subscribe(
      (user: User) => {

        const userTypesHandler = this.userTypes[user.role];
        const type = userTypesHandler();

        this.usersSubject$.next({ users: this.baseEntityService.updateEntity(type, [...this.currentUsers], user) })
      },
      (error) => { }
    );

    return request;
  }

  // POST: Create user
  public createUser(user: User) {

    const userTypesHandler = this.userTypes[user.role];
    const type = userTypesHandler();

    const request = this.baseService.post(`users?language=${localStorage.getItem('lang')}`, user);

    request.subscribe(
      (user: User) => this.usersSubject$.next({ users: this.baseEntityService.updateEntity(type, [...this.currentUsers], user) }),
      (error) => { }
    );

    return request;
  }

  // PUT: Update user
  public updateUser(user: User) {

    const userTypesHandler = this.userTypes[user.role];
    const type = userTypesHandler();

    const request = this.baseService.put(`users/${user._id}`, user);

    request.subscribe(
      (success) => this.usersSubject$.next({ users: this.baseEntityService.updateEntity(type, [...this.currentUsers], user) }),
      (error) => { }
    );

    return request;
  }

  public updateUserPassword(body: { email: string, pass: string }) {
    return this.baseService.put('users', body);
  }

  // DELETE: Delete user
  public deleteUser(userId: string) {

    const request = this.baseService.delete(`users/${userId}`);

    request.subscribe(
      async (success) => {

        const user = this.currentUsers.find(u => u._id === userId);
        if (user && user.role) await this.mediaService.deleteAllFiles('user', user.email);

        this.usersSubject$.next({ users: this.baseEntityService.removeEntity([...this.currentUsers], userId) });
      },
      (error) => { }
    );

    return request;
  }

  // SELECTS

  public selectById(id: string) {
    return this.users$.pipe(
      map((users: User[]) => User.findById(users, id)),
      filter(e => Boolean(e))
    );
  }

  public selectByDni(dni: string) {
    return this.users$.pipe(
      map((users: User[]) => User.findById(users, dni)),
      filter(e => Boolean(e))
    );
  }

  public selectByEmail(email: string) {
    return this.users$.pipe(
      map((users: User[]) => User.findByEmail(users, email)),
      filter(e => Boolean(e))
    );
  }

  public selectByRole(role: number) {
    return this.users$.pipe(
      map((users: User[]) => User.filterByRole(users, role)),
      filter(e => Boolean(e))
    );
  }

  public selectByParent(parent: Parent) {
    return this.users$.pipe(
      map((users: User[]) => Child.filterByParent(users, parent)),
      filter(e => Boolean(e))
    );
  }

  public selectByCamp(camp_id: string) {
    return this.users$.pipe(
      map((users: User[]) => Child.filterByCamp(users, camp_id)),
      filter(e => Boolean(e))
    );
  }
}