import { AuthService } from './auth/auth.service';
export { AuthService } from './auth/auth.service';

import { BaseService } from './base-service/base.service';
export { BaseService } from './base-service/base.service';

import { BillService } from './bill/bill.service';
export { BillService } from './bill/bill.service';

import { CampService } from './camp/camp.service';
export { CampService } from './camp/camp.service';

import { ChartService } from './chart/chart.service';
export { ChartService } from './chart/chart.service';

import { LanguageService } from './language/language.service';
export { LanguageService } from './language/language.service';

import { MediaService } from './media/media.service';
export { MediaService } from './media/media.service';

import { MessageService } from './message/message.service';
export { MessageService } from './message/message.service';

import { ProfileService } from './profile/profile.service';
export { ProfileService } from './profile/profile.service';

import { ProgrammingService } from './programming/programming.service';
export { ProgrammingService } from './programming/programming.service';

import { UserService } from './user/user.service';
export { UserService } from './user/user.service';

export const Services: any[] = [
    AuthService,
    BaseService,
    BillService,
    CampService,
    ChartService,
    LanguageService,
    MediaService,
    MessageService,
    ProfileService,
    ProgrammingService,
    UserService
];