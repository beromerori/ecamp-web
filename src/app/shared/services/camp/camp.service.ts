import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { filter, find, map, pluck } from 'rxjs/operators';

// My models
import { Camp, CampFilters, Child, Parent } from '../../models';

// My services
import { BaseService } from '../base-service/base.service';
import { BaseEntityService } from '../../../utils/base-entity.service';
import { UtilsService } from '../../../utils/utils.service';

export interface State {
  camps?: Camp[];
  total?: number;
}

@Injectable({
  providedIn: 'root'
})
export class CampService {

  public initialState = {
    camps: [],
    total: 0
  }

  private campsSubject$ = new BehaviorSubject<State>(this.initialState);
  public camps$ = this.campsSubject$.asObservable().pipe(pluck('camps'));
  public total$ = this.campsSubject$.asObservable().pipe(pluck('total'));

  constructor(
    private baseEntityService: BaseEntityService,
    private baseService: BaseService,
    private utilsService: UtilsService) { }

  get state() {
    return this.campsSubject$.getValue();
  }

  get currentCamps() {
    return this.campsSubject$.getValue().camps;
  }

  // GET
  public getCamps(filters?: CampFilters, loadMore?: boolean) {

    const request = this.baseService.get('camps', filters);

    request.subscribe(
      (data: { camps: Camp[], total: number }) => {

        data.camps.forEach(camp => {
          camp.modalities = this.utilsService.getCheckedModalities(camp.modalities);
        });

        //data.camps = this.baseEntityService.updateList('camp', [...this.currentCamps], data.camps);

        //if (filters.active) data.camps = this.baseEntityService.createList('camp', data.camps);
        //else data.camps = this.baseEntityService.updateList('camp', [...this.currentCamps], data.camps);

        data.camps = filters.page === 1 ? this.baseEntityService.createList('camp', data.camps) : this.baseEntityService.updateList('camp', [...this.currentCamps], data.camps);

        this.campsSubject$.next({ camps: data.camps, total: data.total })
      },
      (error) => this.campsSubject$.next({ camps: [] })
    );

    return request;
  }

  public getCamp(id: string) {

    const request = this.baseService.get(`camps/${id}`);

    request.subscribe(
      (camp: Camp) => {
        camp.modalities = this.utilsService.getCheckedModalities(camp.modalities);
        this.campsSubject$.next({ camps: this.baseEntityService.updateEntity('camp', [...this.currentCamps], camp) });
      },
      (error) => { }
    );

    return request;
  }

  // POST
  public createCamp(camp: Camp) {

    const request = this.baseService.post('camps', camp);

    request.subscribe(
      (newCamp: Camp) => {

        newCamp.modalities = this.utilsService.getCheckedModalities(newCamp.modalities);
        this.currentCamps.push(this.baseEntityService.createModel('camp', newCamp));

        this.campsSubject$.next({ camps: [...this.currentCamps] });
      },
      (error) => { }
    );

    return request;
  }

  public createApplication(child: Child, parent: Parent, camp: Camp) {

    const request = this.baseService.post(`camps/${camp._id}/applications`, { parent, child, numApplications: camp.numApplications + 1, language: localStorage.getItem('lang') });

    request.subscribe(
      (success) => {
        camp.numApplications = camp.numApplications + 1;
        this.campsSubject$.next({ camps: this.baseEntityService.updateEntity('camp', [...this.currentCamps], camp) });
      },
      (error) => { }
    );

    return request;
  }

  public createBill(user: Parent, camp: Camp) {

    const u = {
      dni: user.dni,
      name: user.name,
      surnames: user.surnames,
      email: user.email,
      phone: user.phone
    };

    const c = {
      camp_id: camp._id,
      campType: camp.type,
      code: camp.code,
      name: camp.name,
      price: camp.price
    };

    return this.baseService.post(`bills`, { user: u, camp: c });
  }

  // PUT
  public updateCamp(camp: Camp) {

    const request = this.baseService.put(`camps/${camp._id}`, camp);

    request.subscribe(
      (success) => {
        camp.modalities = this.utilsService.getCheckedModalities(camp.modalities);
        this.campsSubject$.next({ camps: this.baseEntityService.updateEntity('camp', [...this.currentCamps], camp) })
      },
      (error) => { }
    );

    return request;
  }

  // DELETE
  public deleteCamp(camp: Camp) {

    const request = this.baseService.delete(`camps/${camp._id}`);

    request.subscribe(
      (success) => this.campsSubject$.next({ camps: this.baseEntityService.removeEntity([...this.currentCamps], camp._id) }),
      (error) => { }
    );

    return request;
  }

  // SELECTS

  public selectById(id: string) {
    return this.camps$.pipe(
      map((camps: Camp[]) => Camp.findById(camps, id)),
      find(e => Boolean(e))
    );
  }

  public selectByType(type: number) {
    return this.camps$.pipe(
      map((camps: Camp[]) => Camp.filterByType(camps, type)),
      filter(e => Boolean(e))
    );
  }
}