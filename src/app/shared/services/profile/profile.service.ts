import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { pluck } from 'rxjs/operators';

// My models
import { User } from '../../models';

// My services
import { BaseEntityService } from '../../../utils/base-entity.service';
import { BaseService } from '../base-service/base.service';

export interface State {
  profile?: User
}

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  public initialState = {
    profile: {}
  }

  private profileSubject$ = new BehaviorSubject<State>(this.initialState);
  public profile$ = this.profileSubject$.asObservable().pipe(pluck('profile'));

  constructor(
    private baseEntityService: BaseEntityService,
    private baseService: BaseService) { }

  get state() {
    return this.profileSubject$.getValue();
  }

  get currentProfile() {
    return this.profileSubject$.getValue().profile;
  }

  public getProfile(email: string) {

    const request = this.baseService.get('users/profile', { email });

    request.subscribe(
      (user: User) => {
        if (!user.avatar) user.avatar = { secure_url: 'assets/img/no-img.jpg' };
        this.profileSubject$.next({ profile: this.baseEntityService.createModel('user', user) })
      },
      (error) => { }
    );

    return request;
  }

  public updateProfile(newProfile: User) {

    const request = this.baseService.put(`users/${newProfile._id}`, newProfile);

    request.subscribe(
      (success) => this.profileSubject$.next({ profile: this.baseEntityService.createModel('admin', newProfile) }),
      (error) => { }
    );

    return request;
  }
}