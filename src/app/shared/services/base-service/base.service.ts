import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Rxjs
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(private http: HttpClient) { }

  // GET
  public get(url: string, params?): Observable<any> {
    return this.http.get(url, { params }).pipe(shareReplay());
  }

  // POST
  public post(url: string, body: any): Observable<any> {
    return this.http.post(url, body).pipe(shareReplay());
  }

  // PUT
  public put(url: string, body: any): Observable<any> {
    return this.http.put(url, body).pipe(shareReplay());
  }

  // DELETE
  public delete(url: string, params?): Observable<any> {
    return this.http.delete(url, { params }).pipe(shareReplay());
  }
}