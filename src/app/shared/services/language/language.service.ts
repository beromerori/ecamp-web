import { Injectable } from '@angular/core';
import { DateAdapter } from '@angular/material/core';

// Translate
import { TranslateService } from '@ngx-translate/core';

// Redux
import { Store } from '@ngrx/store';
import { AppStore } from '../../../redux/appStore';
import * as LanguageActions from '../../../redux/actions/language.actions';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(
    private adapter: DateAdapter<any>,
    private store: Store<AppStore>,
    private translate: TranslateService) { }

  public getLanguage() {

    const lang = localStorage.getItem('lang') ? localStorage.getItem('lang') : 'es';
    localStorage.setItem('lang', lang);
    this.store.dispatch(new LanguageActions.SetLanguage(lang));
  }

  public updateLanguage() {

    this.store.select('languageReducer').subscribe(
      (languageReducer) => {

        if (languageReducer && languageReducer.language) {
          this.translate.use(languageReducer.language);
          this.adapter.setLocale(languageReducer.language);
        }
      },
      (error) => { console.error('Error selecting language in store', error); }
    );
  }
}