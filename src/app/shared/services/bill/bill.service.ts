import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { pluck } from 'rxjs/operators';

// My models
import { Bill } from '../../models';

// My services
import { BaseService } from '../base-service/base.service';
import { BaseEntityService } from '../../../utils/base-entity.service';

export interface State {
  bills?: Bill[];
}

@Injectable({
  providedIn: 'root'
})
export class BillService {

  public initialState = {
    bills: []
  }

  private billsSubject$ = new BehaviorSubject<State>(this.initialState);
  public bills$ = this.billsSubject$.asObservable().pipe(pluck('bills'));

  constructor(
    private baseEntityService: BaseEntityService,
    private baseService: BaseService) { }

  get state() {
    return this.billsSubject$.getValue();
  }

  get currentBills() {
    return this.billsSubject$.getValue().bills;
  }

  // GET
  public getBills() {

    const request = this.baseService.get('bills');

    request.subscribe(
      (bills: Bill[]) => this.billsSubject$.next({ bills: this.baseEntityService.createList('bill', bills) }),
      (error) => { }
    );

    return request;
  }
}