import { Injectable } from '@angular/core';

// Rxjs
import { BehaviorSubject } from 'rxjs';
import { find, map, pluck } from 'rxjs/operators';

// My models
import { Programming } from '../../models';

// My services
import { BaseService } from '../base-service/base.service';
import { BaseEntityService } from '../../../utils/base-entity.service';

export interface State {
  programmings?: Programming[];
}

@Injectable({
  providedIn: 'root'
})
export class ProgrammingService {

  public initialState = {
    programmings: []
  }

  private programmingsSubject$ = new BehaviorSubject<State>(this.initialState);
  public programmings$ = this.programmingsSubject$.asObservable().pipe(pluck('programmings'));

  constructor(
    private baseEntityService: BaseEntityService,
    private baseService: BaseService) { }

  get state() {
    return this.programmingsSubject$.getValue();
  }

  get currentProgrammings() {
    return this.programmingsSubject$.getValue().programmings;
  }

  // GET
  public getProgrammings(camp_id: string) {

    const request = this.baseService.get('programmings', { camp_id });

    request.subscribe(
      (programmings: Programming[]) => this.programmingsSubject$.next({ programmings: this.baseEntityService.createList('programming', programmings) }),
      (error) => { }
    );

    return request;
  }

  // POST
  public createProgramming(programming: Programming) {

    const request = this.baseService.post('programmings', programming);

    request.subscribe(
      (newProgramming: Programming) => {
        newProgramming.selected = true;
        this.programmingsSubject$.next({ programmings: this.baseEntityService.updateEntity('programming', [...this.currentProgrammings], newProgramming) })
      },
      (error) => { }
    );

    return request;
  }

  // PUT
  public updateProgramming(updatedProgramming: Programming) {

    const request = this.baseService.put(`programmings/${updatedProgramming._id}`, updatedProgramming);

    request.subscribe(
      (success) => {
        updatedProgramming.selected = true;
        this.programmingsSubject$.next({ programmings: this.baseEntityService.updateEntity('programming', [...this.currentProgrammings], updatedProgramming) })
      },
      (error) => { }
    );

    return request;
  }

  // DELETE
  public deleteProgramming(programming: Programming) {

    const request = this.baseService.delete(`programmings/${programming._id}`);

    request.subscribe(
      (success) => this.programmingsSubject$.next({ programmings: this.baseEntityService.removeEntity([...this.currentProgrammings], programming._id) }),
      (error) => { }
    );

    return request;
  }

  // SELECTS
  public selectById(id: string) {
    return this.programmings$.pipe(
      map((programmings: Programming[]) => Programming.findById(programmings, id)),
      find(e => Boolean(e))
    );
  }
}