import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// My components
import { AlertDialogComponent, AlertPaypalComponent, EcampInfoMessageComponent } from './components';

// My services
import { AlertService } from '../shared/components/alert/alert.service';

/** --------- Angular material modules ---------- */

// Buttons
import { MatButtonModule } from '@angular/material/button';
// Cards
import { MatCardModule } from '@angular/material/card';
// Checkbox
import { MatCheckboxModule } from '@angular/material/checkbox';
// Datepicker
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE } from '@angular/material/core';
// Dialog
import { MatDialogModule } from '@angular/material/dialog';
// Divider
import { MatDividerModule } from '@angular/material/divider';
// Expansion Panel
import { MatExpansionModule } from '@angular/material/expansion';
// Form Field
import { MatFormFieldModule } from '@angular/material/form-field';
// Grid
import { MatGridListModule } from '@angular/material/grid-list';
// Icons
import { MatIconModule } from '@angular/material/icon';
// Input
import { MatInputModule } from '@angular/material/input';
// List
import { MatListModule } from '@angular/material/list';
// Progress Spinner
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// Radio
import { MatRadioModule } from '@angular/material/radio';
// Select
import { MatSelectModule } from '@angular/material/select';
// Sidenav
import { MatSidenavModule } from '@angular/material/sidenav';
// Slider
import { MatSliderModule } from '@angular/material/slider';
// SnackBar
import { MatSnackBarModule } from '@angular/material/snack-bar';
// Sort
import { MatSortModule } from '@angular/material/sort';
// Stepper
import { MatStepperModule } from '@angular/material/stepper';
// Table
import { MatTableModule } from '@angular/material/table';
// Tabs
import { MatTabsModule } from '@angular/material/tabs';
// Toolbar
import { MatToolbarModule } from '@angular/material/toolbar';

/** ---------- Angular material modules ---------- */

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    // Angular material
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule
  ],
  exports: [
    // Angular material
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule
  ],
  entryComponents: [AlertDialogComponent, AlertPaypalComponent, EcampInfoMessageComponent],
  providers: [
    AlertService,
    { provide: MAT_DATE_LOCALE, useValue: 'ja-JP' }
  ]
})
export class MaterialModule { }