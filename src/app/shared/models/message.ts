export class Message {

    _id?: string;
    title?: string;
    body?: boolean;
    date?: {
        timestamp?: number;
        time?: string;
    };
    parent_id?: string;

    constructor(data) {
        Object.assign(this, data);
    }
}