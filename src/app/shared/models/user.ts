import { MediaFile } from "./mediaFile";

export class User {

    _id?: string;
    role?: number;
    isAdmin?: boolean;
    type?: string;
    avatar?: MediaFile;
    dni?: string;
    name?: string;
    surnames?: string;
    sex?: string;
    age?: number;
    birthday?: {
        timestamp?: number;
        time?: string;
    }
    phone?: number;
    email?: string;
    pass?: string;
    address?: string;
    cp?: number;
    location?: string;
    province?: string;

    collapse?: boolean;

    constructor(data) {
        Object.assign(this, data);
        this.collapse = true;
    }

    static findById(users: any[], id: string) {
        return users.find(user => user._id === id);
    }

    static findByDni(users: any[], dni: string) {
        return users.find(user => user.dni === dni);
    }

    static findByEmail(users: any[], email: string) {
        return users.find(user => user.email === email);
    }

    static filterByRole(users: any[], role: number) {
        return users.filter(user => user.role === role);
    }
}

export interface UserCredentials {

    email?: string;
    pass?: string;
}

export interface UserFilters {

    role?: number;
    dni?: string;
    name?: string;
    surnames?: string;
    sex?: string;
    age?: number;
    parent_id?: string;
    camp_id?: string;
    last_id?: string;
    limit?: number;
}

export interface UserLogged {

    status?: number;
    message?: string;
    token?: string;
    isLogged?: boolean;
}