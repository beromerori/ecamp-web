import { User } from './user';

export class Instructor extends User {

    camp?: {
        camp_id?: string;
        campType: number;
        code: string;
        name: string;
    };
    cv?: string;

    constructor(data) {
        super(data);
    }
}