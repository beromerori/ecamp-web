export interface LoggedUser {

    status?: number;
    message?: string;
    token?: string;
}