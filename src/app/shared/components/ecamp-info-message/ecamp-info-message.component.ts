import { Component, Inject, OnInit } from '@angular/core';

import { MAT_SNACK_BAR_DATA } from '@angular/material';

@Component({
  selector: 'app-ecamp-info-message',
  templateUrl: './ecamp-info-message.component.html',
  styleUrls: ['./ecamp-info-message.component.scss']
})
export class EcampInfoMessageComponent implements OnInit {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) { }

  ngOnInit() { }
}