import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-crm-section-title',
  templateUrl: './crm-section-title.component.html',
  styleUrls: ['./crm-section-title.component.scss']
})
export class CrmSectionTitleComponent implements OnInit {

  @Input('title') public sectionTitle: string = '';

  constructor() { }

  ngOnInit() { }
}