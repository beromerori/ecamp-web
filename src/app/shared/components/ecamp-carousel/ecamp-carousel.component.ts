import { Component, Input, OnInit } from '@angular/core';

// Gallery
import { Gallery, GalleryItem, ImageItem } from '@ngx-gallery/core';

// My models
import { MediaFile } from '../../models';

@Component({
  selector: 'app-ecamp-carousel',
  templateUrl: './ecamp-carousel.component.html',
  styleUrls: ['./ecamp-carousel.component.scss']
})
export class EcampCarouselComponent implements OnInit {

  @Input() index: number;
  @Input() images: MediaFile[] = [];

  public id: string;

  private galleryItems: GalleryItem[] = [];

  constructor(private gallery: Gallery) { }

  ngOnInit() {

    this.id = `my-gallery-${this.index}`;

    const galleryRef = this.gallery.ref(`my-gallery-${this.index}`);

    if (!this.images.length) {
      this.galleryItems.push(new ImageItem({ src: 'assets/img/no-img.jpg' }));
    }
    else {
      this.galleryItems = this.images.map((image: MediaFile) => {
        return new ImageItem({ src: image.url });
      });
    }

    galleryRef.load(this.galleryItems);
  }
}