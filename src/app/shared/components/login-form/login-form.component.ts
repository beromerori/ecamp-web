import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// CONSTANTS
import { CONST } from '../../../utils/constants';

// Models
import { UserCredentials } from '../../models';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  @Output('login') public login: EventEmitter<UserCredentials> = new EventEmitter<UserCredentials>();
  @Output() public changePassword: EventEmitter<any> = new EventEmitter<any>();

  public loginForm: FormGroup;
  public hide: boolean = true;
  public iconVisivility: string = 'fas fa-eye';

  constructor() { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {

    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern(CONST.VALIDATIONS.EMAIL_REGEX)]),
      pass: new FormControl('', [Validators.required, Validators.pattern(CONST.VALIDATIONS.PASS_REGEX)])
    });
  }

  public get email() { return this.loginForm.get('email'); }
  public get pass() { return this.loginForm.get('pass'); }

  public getPasswordType() {
    return this.hide ? 'password' : 'text';
  }

  public changeVisibilityPassword() {
    this.hide = !this.hide;
    this.iconVisivility = this.hide ? 'fas fa-eye' : 'fas fa-eye-slash';
  }

  public send() {
    this.login.emit(this.loginForm.value);
  }

  public goToChangePassword() {
    this.changePassword.emit();
  }
}