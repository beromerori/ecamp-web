import { Component, OnInit, Input } from '@angular/core';

// Services
import { LoadingService } from '../../../utils/loading.service';

@Component({
  selector: 'app-ecamp-loading',
  templateUrl: './ecamp-loading.component.html',
  styleUrls: ['./ecamp-loading.component.scss']
})
export class EcampLoadingComponent implements OnInit {

  @Input() isBuildLoader: boolean = false;

  public loading$ = this.loadingService.loading$;

  constructor(private loadingService: LoadingService) { }

  ngOnInit() { }
}