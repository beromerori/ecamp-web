/** ·········· Modals  ·········· */

import { ApplicationModalComponent } from './application-modal/application-modal.component';
export { ApplicationModalComponent } from './application-modal/application-modal.component';

import { BillDetailModalComponent } from './bill-detail-modal/bill-detail-modal.component';
export { BillDetailModalComponent } from './bill-detail-modal/bill-detail-modal.component';

import { CampFiltersModalComponent } from './camp-filters-modal/camp-filters-modal.component';
export { CampFiltersModalComponent } from './camp-filters-modal/camp-filters-modal.component';

import { CampModalComponent } from './camp-modal/camp-modal.component';
export { CampModalComponent } from './camp-modal/camp-modal.component';

import { ChildDetailModalComponent } from './child-detail-modal/child-detail-modal.component';
export { ChildDetailModalComponent } from './child-detail-modal/child-detail-modal.component';

import { ProfileModalComponent } from './profile-modal/profile-modal.component';
export { ProfileModalComponent } from './profile-modal/profile-modal.component';

import { UserModalComponent } from './user-modal/user-modal.component';
export { UserModalComponent } from './user-modal/user-modal.component';

export const Modals: any[] = [
    ApplicationModalComponent,
    BillDetailModalComponent,
    CampFiltersModalComponent,
    CampModalComponent,
    ChildDetailModalComponent,
    ProfileModalComponent,
    UserModalComponent
];