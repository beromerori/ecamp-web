import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Modal
import { DialogRef } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';

// Rxjs
import { tap } from 'rxjs/operators';

// CONST
import { CONST } from '../../../../utils/constants';

// My models
import { Camp, User } from '../../../models';

// My services (barrel)
import { CampService, UserService } from '../../../services';

export class UserModalContext extends BSModalContext {
  user: User;
}

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
export class UserModalComponent implements OnInit {

  // Form
  public userForm: FormGroup;

  // User
  public user;
  public showCv: boolean = false;

  // Camps
  public camps$;

  constructor(
    private dialog: DialogRef<UserModalContext>,
    private campService: CampService) { }

  ngOnInit() {

    this.user = this.dialog.context.user || {};

    if (this.user.cv) this.showCv = Boolean(this.user.cv.secure_url || this.user.cv.url);
    if (this.isInstructor()) this.getCamps(Number(this.user.camp.campType));

    this.initForm();
  }

  private initForm() {

    this.userForm = new FormGroup({
      _id: new FormControl(this.user._id),
      role: new FormControl(this.user.role || 2),
      dni: new FormControl({ value: this.user.dni, disabled: this.user.dni }),
      name: new FormControl(this.user.name, Validators.required),
      surnames: new FormControl(this.user.surnames, Validators.required),
      sex: new FormControl(this.user.sex || 'F', Validators.required),
      age: new FormControl(this.user.age, Validators.required),
      birthday: new FormControl(this.user.birthday),
      phone: new FormControl(this.user.phone),
      email: new FormControl(this.user.email),
      address: new FormControl(this.user.address, Validators.required),
      cp: new FormControl(this.user.cp, Validators.required),
      location: new FormControl(this.user.location, Validators.required),
      province: new FormControl(this.user.province, Validators.required),
      cv: new FormControl(this.user.cv),
      intolerances: new FormControl(this.user.intolerances),
      parent_id: new FormControl(this.user.parent_id)
    });
  }

  get dni() { return this.userForm.get('dni').value }
  get cv() { return this.userForm.get('cv'); }
  get intolerances() { return this.userForm.get('intolerances'); }

  private getCamps(campType: number) {

    this.campService.getCamps({ type: campType });
    this.camps$ = this.campService.selectByType(campType);
  }

  public isInstructor() {
    return this.user.role === CONST.ROLES.INSTRUCTOR;
  }

  public isParent() {
    return this.user.role === CONST.ROLES.PARENT;
  }

  public isChild() {
    return !this.user.role;
  }

  public setCv($event) {

    const vm = this;
    const fileReader = new FileReader();

    fileReader.onload = function () {
      vm.cv.patchValue(fileReader.result);
      vm.showCv = false;
    }

    fileReader.readAsDataURL($event.target.files[0]);
  }

  public openCv() {
    window.open(this.user.cv.secure_url || this.user.cv.url);
  }

  public changeCampType($event) {

    const campType = Number($event.value);
    this.getCamps(campType);
  }

  public selectCamp($event, camps: Camp[]) {

    const camp_id = $event.value;

    const camp = camps.find(c => c._id === camp_id);

    if (camp) {
      this.user.camp.code = camp.code;
      this.user.camp.name = camp.name;
    }
  }

  public save() {

    let user = { ...this.userForm.value, dni: this.dni };

    if (this.user.childs_ids && this.user.childs_ids.length) user.childs_ids = this.user.childs_ids;
    if (this.user.camp && this.user.camp.camp_id) user.camp = this.user.camp;

    this.dialog.close(user);
  }

  public cancel() {
    this.dialog.close();
  }
}