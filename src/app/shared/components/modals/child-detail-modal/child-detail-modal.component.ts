import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

// Modal
import { DialogRef } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';

// Moment
import * as moment from 'moment';

// My Models
import { Child, Message, Parent } from '../../../models';

// My services (barrel)
import { MessageService, UserService } from '../../../services';
import { UtilsService } from '../../../../utils/utils.service';

export class ChildDetailModalContext extends BSModalContext {
  child: Child;
}

@Component({
  selector: 'app-child-detail-modal',
  templateUrl: './child-detail-modal.component.html',
  styleUrls: ['./child-detail-modal.component.scss']
})
export class ChildDetailModalComponent implements OnInit {

  public messageForm: FormGroup;

  public child: Child;
  public parent: Parent;

  constructor(
    private dialog: DialogRef<ChildDetailModalContext>,
    private messageService: MessageService,
    private userService: UserService,
    private utilsService: UtilsService) { }

  ngOnInit() {

    this.child = this.dialog.context.child;

    if (this.child) this.getParentInfo();

    this.initForm();
  }

  private getParentInfo() {

    this.userService.getUser(this.child.parent_id).subscribe(
      (parent: Parent) => this.parent = parent,
      (error) => this.utilsService.openErrorAlert(error.error)
    );
  }

  private initForm() {

    this.messageForm = new FormGroup({
      title: new FormControl('', Validators.required),
      body: new FormControl('', Validators.required),
      parent_id: new FormControl(this.child ? this.child.parent_id : '')
    });
  }

  public sendMessage() {

    if (this.messageForm.invalid) return;

    const message: Message = { ...this.messageForm.value, date: { time: moment().locale('es').format('L'), timestamp: moment().valueOf() } };

    this.messageService.sendMessage(message).subscribe(
      (success) => {
        this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.MESSAGE.CREATED');
        this.dialog.close();
      },
      (error) => this.utilsService.openErrorAlert(error.error)
    );
  }

  public cancel() {
    this.dialog.close();
  }
}