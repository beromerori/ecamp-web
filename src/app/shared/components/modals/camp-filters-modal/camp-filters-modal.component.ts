import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Modal
import { DialogRef } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';

// Moment
import * as moment from 'moment';

// CONST
import { CONST } from '../../../../utils/constants';

// My models
import { CampFilters, Modality } from '../../../models';

export class CampFiltersModalContext extends BSModalContext {
  filters: CampFilters;
}

@Component({
  selector: 'app-camp-filters-modal',
  templateUrl: './camp-filters-modal.component.html',
  styleUrls: ['./camp-filters-modal.component.scss']
})
export class CampFiltersModalComponent implements OnInit {

  public filters: CampFilters = {};
  public filtersForm: FormGroup;

  // Location
  public autonomousCommunities = CONST.AUTONOMOUS_COMMUNITIES;
  public provinces: string[] = [];

  // Modalities
  public allModalities: Modality[] = JSON.parse(JSON.stringify(CONST.MODALITIES));
  private modalitiesIds: number[] = [];

  constructor(private dialog: DialogRef<CampFiltersModalContext>) { }

  ngOnInit() {

    this.filters = this.dialog.context.filters;

    this.initFiltersForm();
    this.fillProvinces();
    this.checkModalities();
  }

  private initFiltersForm() {

    this.filtersForm = new FormGroup({
      active: new FormControl(this.filters.active),
      name: new FormControl(this.filters.name),
      startDate: new FormControl(moment(this.filters.startDate)),
      endDate: new FormControl(this.filters.endDate ? moment(this.filters.endDate) : ''),
      ccaa: new FormControl(this.filters.ccaa ? this.filters.ccaa : ''),
      province: new FormControl(this.filters.province),
      maxAge: new FormControl(this.filters.maxAge),
      price: new FormControl(this.filters.price),
      modalities: new FormControl(this.filters.modalities)
    });

    this.ccaa.valueChanges.subscribe(
      (id: number) => this.provinces = id ? this.autonomousCommunities[id].provinces : []
    );
  }

  get active() { return this.filtersForm.get('active'); };
  get name() { return this.filtersForm.get('name'); };
  get startDate() { return this.filtersForm.get('startDate'); };
  get endDate() { return this.filtersForm.get('endDate'); };
  get ccaa() { return this.filtersForm.get('ccaa'); };
  get province() { return this.filtersForm.get('province'); };
  get maxAge() { return this.filtersForm.get('maxAge'); };
  get price() { return this.filtersForm.get('price'); };
  get modalities() { return this.filtersForm.get('modalities'); };

  // Dates
  public isValidDate() {
    return this.startDate.value <= this.endDate.value;
  }

  // Location

  private fillProvinces() {
    this.provinces = this.ccaa.value ? CONST.AUTONOMOUS_COMMUNITIES[this.ccaa.value].provinces : [];
  }

  // Modalities

  private checkModalities() {

    this.modalitiesIds = this.modalities.value ? this.modalities.value.split(',') : [];

    this.modalitiesIds.forEach(id => this.allModalities[id].checked = true);
  }

  public selectModality(modality: Modality) {

    this.allModalities[modality.id].checked = !this.allModalities[modality.id].checked;

    const index = this.modalitiesIds.findIndex(id => id == modality.id);

    if (index !== -1) this.modalitiesIds.splice(index, 1);
    else this.modalitiesIds.push(modality.id);
  }

  public applyFilters() {

    this.active.patchValue(true);
    this.startDate.patchValue(moment(this.startDate.value).valueOf());
    this.endDate.patchValue(moment(this.endDate.value).valueOf());
    this.modalities.patchValue(this.modalitiesIds.toString());

    for (let field in this.filtersForm.value) {
      if (!this.filtersForm.value[field]) {
        delete this.filtersForm.value[field];
      }
    }

    this.dialog.close(this.filtersForm.value);
  }

  public clearFilters() {
    this.dialog.close({});
  }

  public closeModal() {
    this.dialog.close();
  }
}