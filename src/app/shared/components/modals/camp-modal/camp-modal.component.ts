import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

// Modal
import { DialogRef } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';

// Moment
import * as moment from 'moment';

// CONST
import { CONST } from '../../../../utils/constants';

// My models
import { Camp } from '../../../models';

// My services
import { UtilsService } from '../../../../utils/utils.service';

export class CampModalContext extends BSModalContext {
    camp: Camp;
}

@Component({
    selector: 'app-camp-modal',
    templateUrl: './camp-modal.component.html',
    styleUrls: ['./camp-modal.component.scss'],
    animations: []
})
export class CampModalComponent implements OnInit {

    // Camp
    public camp: Camp;
    public minDate = moment(new Date());
    public maxDate = moment(new Date());
    public autonomousCommunities = CONST.AUTONOMOUS_COMMUNITIES;
    public provinces: String[];

    // Forms
    public step1Form: FormGroup;
    public step2Form: FormGroup;
    public step4Form: FormGroup;

    public showPDF: boolean = false;

    constructor(
        private dialog: DialogRef<CampModalContext>,
        private utilsService: UtilsService) { }

    ngOnInit() {

        this.camp = this.dialog.context.camp;

        this.showPDF = Boolean(this.camp.pdf && (this.camp.pdf.secure_url || this.camp.pdf.url));

        this.fillProvinces();

        this.initStep1Form();
        this.initStep2Form();
        this.initStep4Form();
    }

    // Step 1: Data

    private initStep1Form() {

        this.step1Form = new FormGroup({
            _id: new FormControl(this.camp._id),
            type: new FormControl(this.camp.type, [Validators.required]),
            code: new FormControl({ value: this.camp.code, disabled: this.camp.code }, Validators.required),
            name: new FormControl(this.camp.name, [Validators.required]),
            startDate: new FormControl(this.getStartDate(), Validators.required),
            endDate: new FormControl(this.getEndDate(), Validators.required),
            minAge: new FormControl(this.camp.age.min, [Validators.required, Validators.min(12), Validators.max(18)]),
            maxAge: new FormControl(this.camp.age.max, [Validators.required, Validators.min(12), Validators.max(18)]),
            numPeople: new FormControl(this.camp.numPeople, [Validators.required, Validators.min(10), Validators.max(50)]),
            price: new FormControl(this.camp.price, [Validators.required])
        });

        this.step1Form.get('startDate').valueChanges.subscribe(
            (date) => {
                this.maxDate = date;
                this.step1Form.get('endDate').patchValue(this.maxDate);
            }
        );
    }

    public isAutonomousCamp() {
        return this.camp.type === CONST.CAMP_TYPES.AUTONOMOUS;
    }

    public isExchangeCamp() {
        return this.camp.type === CONST.CAMP_TYPES.EXCHANGE;
    }

    private getStartDate() {
        return this.camp.date.start.timestamp ? moment(this.camp.date.start.timestamp) : moment(new Date());
    }

    private getEndDate() {
        return this.camp.date.end.timestamp ? moment(this.camp.date.end.timestamp) : moment(new Date());
    }

    private getNumDays() {
        const start = moment(this.camp.date.start.timestamp);
        const end = moment(this.camp.date.end.timestamp);
        return end.diff(start, 'days');
    }

    // Step 2: Installations

    private initStep2Form() {

        this.step2Form = new FormGroup({
            installations: new FormControl(this.camp.installations, [Validators.required]),
            address: new FormControl(this.camp.address, [Validators.required]),
            ccaa: new FormControl(parseInt(this.camp.ccaa.toString()), [Validators.required]),
            province: new FormControl(this.getProvince(), [Validators.required]),
            cp: new FormControl(this.camp.cp, [Validators.required]),
        });

        this.step2Form.get('ccaa').valueChanges.subscribe(
            (id: number) => {
                this.provinces = this.autonomousCommunities[id].provinces;
            }
        );
    }

    private fillProvinces() {
        this.provinces = CONST.AUTONOMOUS_COMMUNITIES[this.camp.ccaa].provinces;
    }

    private getProvince() {
        return this.camp.province ? this.camp.province : this.provinces[0];
    }

    // Step 3: Images & PDF
    public updateImages($event) {
        this.camp.images = $event;
    }

    public addPDF($event) {

        const vm = this;
        const fileReader = new FileReader();

        fileReader.onload = function () {
            vm.camp.pdf = { url: fileReader.result.toString() };
            vm.showPDF = false;
        }

        fileReader.readAsDataURL($event.target.files[0]);
    }

    public openPDF() {
        window.open(this.camp.pdf.secure_url || this.camp.pdf.url);
    }

    // Step 4: Others

    private initStep4Form() {

        this.step4Form = new FormGroup({
            description: new FormControl(this.camp.description, Validators.required),
            observations: new FormControl(this.camp.observations)
        });
    }

    public selectModality(id: number) {
        this.camp.modalities[id].checked = !this.camp.modalities[id].checked;
    }

    public save() {

        this.camp = {
            _id: this.step1Form.value._id,
            type: this.step1Form.value.type,
            code: this.step1Form.get('code').value.replace(/ /g, ''),
            name: this.step1Form.value.name,
            description: this.step4Form.value.description,
            images: this.camp.images,
            pdf: this.camp.pdf,
            ccaa: this.step2Form.value.ccaa,
            province: this.step2Form.value.province,
            cp: this.step2Form.value.cp,
            address: this.step2Form.value.address,
            installations: this.step2Form.value.installations,
            date: {
                start: { time: this.step1Form.value.startDate.locale('es').format('l'), timestamp: this.step1Form.value.startDate.add(3, 'hours').valueOf() },
                end: { time: this.step1Form.value.endDate.locale('es').format('l'), timestamp: this.step1Form.value.endDate.add(3, 'hours').valueOf() }
            },
            numPeople: this.step1Form.value.numPeople,
            numApplications: this.camp.numApplications,
            age: { min: this.step1Form.value.minAge, max: this.step1Form.value.maxAge },
            price: this.step1Form.value.price,
            modalities: this.utilsService.getModalitiesIds(this.camp),
            observations: this.step4Form.value.observations
        }

        if (!this.camp._id) delete this.camp._id;
        this.camp.numDays = this.getNumDays();
        this.dialog.close(this.camp);
    }

    public closeModal() {
        this.dialog.close();
    }
}