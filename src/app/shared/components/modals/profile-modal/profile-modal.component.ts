import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// CONSTANTS
import { CONST } from '../../../../utils/constants';

// Modal
import { DialogRef } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';

// My Models
import { User } from '../../../models';

export class ProfileModalContext extends BSModalContext {
  user: User;
}

@Component({
  selector: 'app-profile-modal',
  templateUrl: './profile-modal.component.html',
  styleUrls: ['./profile-modal.component.scss']
})
export class ProfileModalComponent implements OnInit {

  // User
  public user;
  public showCv: boolean = false;

  // Form
  public profileForm: FormGroup;

  public passwordsInvalid: boolean = false;

  constructor(private dialog: DialogRef<ProfileModalContext>) { }

  ngOnInit() {

    this.user = this.dialog.context.user;

    if (this.user.cv) this.showCv = Boolean(this.user.cv.secure_url || this.user.cv.url);

    this.initForm();
  }

  private initForm() {

    this.profileForm = new FormGroup({
      _id: new FormControl(this.user._id),
      avatar: new FormControl(this.user.avatar),
      name: new FormControl(this.user.name, Validators.required),
      surnames: new FormControl(this.user.surnames, Validators.required),
      email: new FormControl(this.user.email, [Validators.required, Validators.pattern(CONST.VALIDATIONS.EMAIL_REGEX)]),
      pass: new FormGroup({
        firstPass: new FormControl('', Validators.pattern(CONST.VALIDATIONS.PASS_REGEX)),
        repeatPass: new FormControl('', Validators.pattern(CONST.VALIDATIONS.PASS_REGEX))
      }, this.areEquals),
      cv: new FormControl(this.user.cv),
    });

    this.firstPass.valueChanges.subscribe(
      (pass: string) => this.passwordsInvalid = pass && (pass !== this.repeatPass.value)
    );

    this.repeatPass.valueChanges.subscribe(
      (pass: string) => this.passwordsInvalid = pass && (pass !== this.firstPass.value)
    );
  }

  get email(): any { return this.profileForm.get('email'); }
  get firstPass() { return this.profileForm.get('pass').get('firstPass'); }
  get repeatPass() { return this.profileForm.get('pass').get('repeatPass'); }
  get cv() { return this.profileForm.get('cv'); }

  private areEquals(form: any) {

    const fields: string[] = [];

    for (let control in form.controls) {
      fields.push(control);
    }

    return form.controls[`${fields[0]}`].value === form.controls[`${fields[1]}`].value ? null : { equal: { valid: false } };
  }
  public isInstructor() {
    return this.user.role === CONST.ROLES.INSTRUCTOR;
  }

  public setCv($event) {

    const vm = this;
    const fileReader = new FileReader();

    fileReader.onload = function () {
      vm.cv.patchValue(fileReader.result);
      vm.showCv = false;
    }

    fileReader.readAsDataURL($event.target.files[0]);
  }

  public openCv() {
    window.open(this.user.cv.secure_url || this.user.cv.url);
  }

  public save() {
    this.profileForm.value.email = this.email.value;
    this.profileForm.value.pass = this.firstPass.value;
    this.dialog.close(this.profileForm.value);
  }

  public cancel() {
    this.dialog.close();
  }
}