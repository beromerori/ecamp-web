import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

// Modal
import { DialogRef } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';

// Moment
import * as moment from 'moment';

// Models
import { Camp } from '../../../models';

export class ApplicationModalContext extends BSModalContext {
  camp: Camp;
}

@Component({
  selector: 'app-application-modal',
  templateUrl: './application-modal.component.html',
  styleUrls: ['./application-modal.component.scss']
})
export class ApplicationModalComponent implements OnInit {

  public childForm: FormGroup;
  public parentForm: FormGroup;

  public camp: Camp;

  public validAge: boolean = true;

  constructor(private dialog: DialogRef<ApplicationModalContext>) { }

  ngOnInit() {

    this.camp = this.dialog.context.camp;

    this.initChildForm();
    this.initParentForm();
  }

  // Child

  private initChildForm() {

    this.childForm = new FormGroup({
      birthday: new FormControl('', Validators.required),
      dni: new FormControl(''),
      name: new FormControl('', Validators.required),
      surnames: new FormControl('', Validators.required),
      sex: new FormControl('F', Validators.required),
      age: new FormControl(),
      address: new FormControl('', Validators.required),
      cp: new FormControl(Number(''), Validators.required),
      location: new FormControl('', Validators.required),
      province: new FormControl('', Validators.required),
      role: new FormControl(null),
      intolerances: new FormControl('')
    });

    this.birthday.valueChanges.subscribe(
      (birthday) => this.validAge = this.checkAge(birthday)
    );
  }

  get birthday() { return this.childForm.get('birthday'); }
  get age() { return this.childForm.get('age'); }
  get address() { return this.childForm.get('address'); }
  get cp() { return this.childForm.get('cp'); }
  get location() { return this.childForm.get('location'); }
  get province() { return this.childForm.get('province'); }

  private checkAge(birthday) {

    const ageCampFinish = moment(this.camp.date.end.timestamp).diff(birthday, 'years');
    return (ageCampFinish >= this.camp.age.min) && (ageCampFinish <= this.camp.age.max);
  }

  // Parent

  private initParentForm() {

    this.parentForm = new FormGroup({
      dni: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      surnames: new FormControl('', Validators.required),
      sex: new FormControl('F', Validators.required),
      age: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      cp: new FormControl(Number(''), Validators.required),
      location: new FormControl('', Validators.required),
      province: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      role: new FormControl(1),
      dataProtection: new FormControl(false, Validators.requiredTrue),
      authorization: new FormControl(false, Validators.requiredTrue)
    });
  }

  public send() {

    this.age.patchValue(moment().diff(moment(this.birthday.value), 'years'));

    this.birthday.patchValue({
      timestamp: moment(this.birthday.value).valueOf(),
      time: moment(this.birthday.value).locale('es').format('l')
    });

    const child = { ...this.childForm.value, camp: { camp_id: this.camp._id, campType: this.camp.type, code: this.camp.code, name: this.camp.name } };

    this.dialog.close({ child, parent: this.parentForm.value });
  }

  public closeModal() {
    this.dialog.close();
  }
}