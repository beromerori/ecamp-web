import { Component, OnInit } from '@angular/core';

// Modal
import { DialogRef } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';

// My Models
import { Bill } from '../../../models';

export class BillDetailModalContext extends BSModalContext {
  bill: Bill;
}

@Component({
  selector: 'app-bill-detail-modal',
  templateUrl: './bill-detail-modal.component.html',
  styleUrls: ['./bill-detail-modal.component.scss']
})
export class BillDetailModalComponent implements OnInit {

  // Bill
  public bill: Bill;

  constructor(private dialog: DialogRef<BillDetailModalContext>) { }

  ngOnInit() {
    this.bill = this.dialog.context.bill;
  }

  public cancel() {
    this.dialog.close();
  }
}