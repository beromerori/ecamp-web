import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ecamp-input-error',
  templateUrl: './ecamp-input-error.component.html',
  styleUrls: ['./ecamp-input-error.component.scss']
})
export class EcampInputErrorComponent implements OnInit {

  @Input() public control;
  @Input() public message;

  constructor() { }

  ngOnInit() { }
}