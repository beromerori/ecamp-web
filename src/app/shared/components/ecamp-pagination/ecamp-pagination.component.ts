import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';

@Component({
  selector: 'app-ecamp-pagination',
  templateUrl: './ecamp-pagination.component.html',
  styleUrls: ['./ecamp-pagination.component.scss']
})
export class EcampPaginationComponent implements OnChanges {

  @Input() itemsPerPage: number;
  @Input() itemsTotal: number;

  @Output() previousPage: EventEmitter<number> = new EventEmitter<number>();
  @Output() nextPage: EventEmitter<number> = new EventEmitter<number>();

  public page: number = 1;
  public totalPages: number;

  constructor() { }

  ngOnChanges() {
    this.totalPages = Math.ceil(this.itemsTotal / this.itemsPerPage);
  }

  public isFirstPage() {
    return this.page === 1;
  }

  public isLastPage() {
    return this.page === this.totalPages;
  }

  public subtractPage() {
    this.page = this.page > 1 ? this.page - 1 : 1;
    this.previousPage.emit(this.page);
  }

  public addPage() {
    this.page = this.page < this.totalPages ? this.page + 1 : this.totalPages;
    this.nextPage.emit(this.page);
  }
}