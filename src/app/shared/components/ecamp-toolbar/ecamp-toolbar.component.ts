import { Component } from '@angular/core';
import { Router } from '@angular/router';

// Redux
import { Store } from '@ngrx/store';
import { AppStore } from '../../../redux/appStore';
import * as LanguageActions from '../../../redux/actions/language.actions';

// CONST
import { CONST } from '../../../utils/constants';

// My services (barrel)
import { AuthService } from '../../services';

@Component({
  selector: 'app-ecamp-toolbar',
  templateUrl: './ecamp-toolbar.component.html',
  styleUrls: ['./ecamp-toolbar.component.scss']
})
export class EcampToolbarComponent {

  public lang: string = localStorage.getItem('lang');
  public languages: Array<{ id: number, name: string, code: string, icon: string }> = [];

  public userLogged$ = this.authService.userLogged$;

  constructor(
    private router: Router,
    private store: Store<AppStore>,
    private authService: AuthService) {

    this.languages = CONST.LANGUAGES;
  }

  public setLanguage($event) {
    this.lang = $event;
    localStorage.setItem('lang', this.lang);
    this.store.dispatch(new LanguageActions.SetLanguage(this.lang));
  }

  public goToHome() {
    this.router.navigate(['ecamp/web/camps']);
  }

  public goToAdmin() {
    this.router.navigate(['ecamp/crm/camps'], { queryParams: { type: CONST.CAMP_TYPES.AUTONOMOUS } });
  }

  public logout() {
    this.authService.logout();
    this.router.navigate(['ecamp/crm/login']);
  }
}