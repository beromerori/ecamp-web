import { Injectable } from '@angular/core';

// Angular Material
import { MatDialog } from '@angular/material/dialog';

// My components (barrel)
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { AlertPaypalComponent } from './alert-paypal/alert-paypal.component';

// My models
import { Camp } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(public dialog: MatDialog) { }

  public createAlert(type: number, title: string, subtitle: string) {

    return this.dialog.open(AlertDialogComponent, {
      data: {
        type: type,
        title: title,
        subtitle: subtitle
      }
    });
  }

  public createPaypalAlert(camp: Camp) {

    return this.dialog.open(AlertPaypalComponent, {
      disableClose: true,
      data: {
        camp
      }
    });
  }

  public presentErrorAlert(error: { status: number, message: string, statusText?: string }) {

    if (error.status === 502) return;
    if (!error.status) error.message = 'SERVER.ERRORS.NOT-CONNECTION';

    return new Promise((resolve, reject) => {

      const alert = this.dialog.open(AlertDialogComponent, {
        data: {
          type: 3,
          title: 'ALERT.OOPS',
          subtitle: error.message
        }
      });

      alert.afterClosed().subscribe(
        (success) => resolve(),
        (error) => reject()
      );
    });
  }
}