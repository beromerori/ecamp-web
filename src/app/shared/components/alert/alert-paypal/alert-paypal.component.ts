import { AfterViewChecked, Component, Inject, OnInit } from '@angular/core';

// Angular Material
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

// CONST
import { CONST } from '../../../../utils/constants';

// ENV
import { environment } from '../../../../../environments/environment.dev';

// My models
import { Camp } from '../../../models';

declare var paypal: any;

export interface PaypalData {
  camp: Camp
}

@Component({
  selector: 'app-alert-paypal',
  templateUrl: './alert-paypal.component.html',
  styleUrls: ['./alert-paypal.component.scss']
})
export class AlertPaypalComponent implements AfterViewChecked, OnInit {

  private addScript: boolean = false;

  private amount: number = 10;

  /*private paypalConfig = {
    env: CONST.PAYPAL.ENV[0],
    client: {
      sandbox: environment.paypal_client_id,
      production: environment.paypal_client_id
    },
    commit: true,
    payment: (data, actions) => {

      return actions.payment.create({
        payment: {
          transactions: [
            { amount: { total: this.amount, currency: CONST.PAYPAL.CURRENCY } }
          ]
        }
      });
    },
    onAuthorize: (data, actions) => {

      return actions.payment.execute()
        .then((payment) => {
          this.dialogRef.close(payment);
        });
    }
  };*/

  constructor(
    public dialogRef: MatDialogRef<AlertPaypalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: PaypalData) { }

  ngOnInit() {
    this.amount = this.data.camp.price;
  }

  async ngAfterViewChecked() {

    if (!this.addScript) {

      await this.loadPaypal();
    }
  }

  // Paypal

  private paypalConfig = {
    env: environment.paypal_env,
    client: {
      sandbox: environment.paypal_client_id,
      production: environment.paypal_client_id
    },
    style: {
      color: 'blue',
      shape: 'pill',
      label: 'pay',
      size: 'responsive',
      layout: 'horizontal'
    },
    commit: true
  };

  private loadPaypal(): Promise<boolean> {

    const vm = this;

    return new Promise((resolve, reject) => {

      this.addPaypalScript()
        .then(() => {
          paypal.Buttons({
            ...this.paypalConfig,
            createOrder: function (data, actions) {
              return actions.order.create({
                purchase_units: [{
                  amount: {
                    value: vm.amount.toString()
                  }
                }]
              });
            },
            // Finalize the transaction
            onApprove: function (data, actions) {
              return actions.order.capture().then(function (details) {
                vm.dialogRef.close(details);
              });
            }
          }).render('#paypal-btn');
        });

      resolve(true);
    });
  }

  private addPaypalScript() {

    this.addScript = true;

    return new Promise((resolve, reject) => {
      let scriptTagElement = document.createElement('script');
      scriptTagElement.src = environment.paypal_script;
      scriptTagElement.onload = resolve;
      document.body.appendChild(scriptTagElement);
    });
  }
}