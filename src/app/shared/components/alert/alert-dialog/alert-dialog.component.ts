import { Component, Inject } from '@angular/core';

// Angular Material
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

// CONSTANTS
import { CONST } from '../../../../utils/constants';

export interface DialogData {
  type: 0,
  title: '',
  subtitle: ''
}

@Component({
  selector: 'app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.scss']
})
export class AlertDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  public isConfirmAlert() {
    return this.data.type === CONST.ALERT_TYPES.CONFIRM;
  }

  public cancel() { }
}