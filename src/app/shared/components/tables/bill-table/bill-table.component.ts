import { Component, Input, OnInit } from '@angular/core';

// Modal
import { overlayConfigFactory, Modal } from 'ngx-modialog';

// My components (barrel)
import { BillDetailModalComponent } from '../../modals/bill-detail-modal/bill-detail-modal.component';

// My Models
import { Bill } from '../../../models';

@Component({
  selector: 'app-bill-table',
  templateUrl: './bill-table.component.html',
  styleUrls: ['./bill-table.component.scss']
})
export class BillTableComponent implements OnInit {

  @Input() public bills: Bill[] = [];

  constructor(private modal: Modal) { }

  ngOnInit() { }

  public showBill(bill: Bill) {

    const billDetailModal = this.modal.open(BillDetailModalComponent, overlayConfigFactory({
      dialogClass: 'ecamp-modal-sm ecamp-modal',
      bill: bill
    }));
  }
}