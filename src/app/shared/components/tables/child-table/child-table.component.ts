import { Component, Input, OnInit } from '@angular/core';

// Modal
import { overlayConfigFactory, Modal } from 'ngx-modialog';

// My components (barrel)
import { ChildDetailModalComponent } from '../../modals';

// My models
import { Child } from '../../../../shared/models';

@Component({
  selector: 'app-child-table',
  templateUrl: './child-table.component.html',
  styleUrls: ['./child-table.component.scss']
})
export class ChildTableComponent implements OnInit {

  @Input() childs: Child[] = [];

  constructor(private modal: Modal) { }

  ngOnInit() {}

  public showChild(child: Child) {

    const childDetailModal = this.modal.open(ChildDetailModalComponent, overlayConfigFactory({
      dialogClass: 'ecamp-modal-sm ecamp-modal',
      child: child
    }));
  }
}