/** ·········· Tables  ·········· */

// Bill
import { BillTableComponent } from './bill-table/bill-table.component';
export { BillTableComponent } from './bill-table/bill-table.component';

// Child
import { ChildTableComponent } from './child-table/child-table.component';
export { ChildTableComponent } from './child-table/child-table.component';

// User
import { UserTableComponent } from './user-table/user-table.component';
export { UserTableComponent } from './user-table/user-table.component';

export const Tables: any[] = [
    BillTableComponent,
    ChildTableComponent,
    UserTableComponent
];