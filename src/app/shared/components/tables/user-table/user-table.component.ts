import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

// Modal
import { Modal, overlayConfigFactory } from 'ngx-modialog';

// CONST
import { CONST } from '../../../../utils/constants';

// My Components (barrel)
import { UserModalComponent } from '../../modals';

// My Models
import { Instructor, Parent, User } from '../../../models';

// My Services (barrel)
import { UserService } from '../../../services/user/user.service';
import { UtilsService } from '../../../../utils/utils.service';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss'],
  animations: [
    trigger('detailExpand',
      [
        state('collapsed', style({ height: '0px', minHeight: '0px', display: 'none' })),
        state('expanded', style({ height: '*' })),
        transition('expanded <=> collapsed', animate('150ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
      ]
    )
  ]
})

export class UserTableComponent implements OnInit {

  @Input() private role: number;
  @Input() public users: User[] = [];

  @Output() editUser: EventEmitter<{ updatedUser: User, oldCV: any }> = new EventEmitter<{ updatedUser: User, oldCV: any }>();
  @Output() deleteUser: EventEmitter<any> = new EventEmitter<User>();

  public parentSelectors = {};
  public instructorSelectors = {};

  constructor(
    private modal: Modal,
    private userService: UserService,
    private utilsService: UtilsService) { }

  ngOnInit() { }

  public isInstructor() {
    return this.role === CONST.ROLES.INSTRUCTOR;
  }

  public isParent() {
    return this.role === CONST.ROLES.PARENT;
  }

  public isChild() {
    return !this.role;
  }

  public getIcon(user: User) {
    return user.collapse ? 'fas fa-angle-right' : 'fas fa-angle-down';
  }

  public expandUser(user) {

    user.collapse = !user.collapse;

    if (user.collapse) return;

    if (this.isParent()) this.getChilds(user);
  }

  private getChilds(parent: Parent) {

    this.userService.getChilds(parent).subscribe(
      (success) => this.parentSelectors[parent._id] = this.userService.selectByParent(parent),
      (error) => this.utilsService.openErrorAlert(error.error)
    );
  }

  // Update user
  public edit(user) {

    const userModal = this.modal.open(UserModalComponent, overlayConfigFactory({
      dialogClass: 'ecamp-modal-md ecamp-modal',
      user: user
    }));

    const oldUser = JSON.parse(JSON.stringify(user));

    userModal.result.then((user) => {

      if (!user) return;

      if (user.cv && oldUser.cv && ((user.cv.secure_url === oldUser.cv.secure_url) || (user.cv.url === oldUser.cv.url))) delete user.cv;

      this.editUser.emit({ updatedUser: user, oldCV: oldUser.cv });
    });
  }

  // Delete user
  public removeUser(user) {

    const alert = this.utilsService.openAlert(1, 'ALERT.REMOVE-USER-ANSWER');

    alert.afterClosed().subscribe((deleted: boolean) => {

      if (deleted) this.deleteUser.emit(user);
    });
  }
}