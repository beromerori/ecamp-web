import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modality-badge',
  templateUrl: './modality-badge.component.html',
  styleUrls: ['./modality-badge.component.scss']
})
export class ModalityBadgeComponent implements OnInit {

  @Input() id: number;
  @Input() name: string;
  @Input() checked: boolean;
  @Input() canCheck: boolean;

  public type: string;

  constructor() { }

  ngOnInit() {
    this.type = !this.canCheck || this.checked ? `modality-${this.id}` : `modality-${this.id}-disabled`;
  }

  public setType() {
    this.checked = !this.checked;
    this.type = this.checked ? `modality-${this.id}` : `modality-${this.id}-disabled`;
  }
}