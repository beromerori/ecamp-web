import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Modal
import { overlayConfigFactory, Modal } from 'ngx-modialog';

// CONST
import { CONST } from '../../../utils/constants';

// My components (barrel)
import { ProfileModalComponent } from '../modals';

// My models
import { MediaFile, User } from '../../models';

// My services (barrel)
import { MediaService, ProfileService } from '../../services';
import { UtilsService } from '../../../utils/utils.service';

@Component({
  selector: 'app-crm-navigation',
  templateUrl: './crm-navigation.component.html',
  styleUrls: ['./crm-navigation.component.scss']
})
export class CrmNavigationComponent implements OnInit {

  public profile$ = this.profileService.profile$;

  public isAdmin: boolean = false;
  public isInstructor: boolean = false;

  constructor(
    private modal: Modal,
    private router: Router,
    private mediaService: MediaService,
    private profileService: ProfileService,
    private utilsService: UtilsService) { }

  ngOnInit() {
    this.getUser();
  }

  private getUser() {

    const email = sessionStorage.getItem('email');

    this.profileService.getProfile(email).subscribe(
      (user) => {

        if (user.camp_id) sessionStorage.setItem('camp_id', user.camp_id);

        this.isAdmin = user.role === CONST.ROLES.ADMIN;
        this.isInstructor = user.role === CONST.ROLES.INSTRUCTOR;
      },
      (error) => this.utilsService.openErrorAlert(error.error)
    );
  }

  public editProfile(profile) {

    const profileModal = this.modal.open(ProfileModalComponent, overlayConfigFactory({
      dialogClass: 'ecamp-modal-md ecamp-modal',
      user: profile
    }));

    const oldUser = JSON.parse(JSON.stringify(profile));

    profileModal.result.then(async (newProfile) => {

      if (!newProfile) return;

      if (!newProfile.pass) delete newProfile.pass;

      if (newProfile.cv && oldUser.cv && ((newProfile.cv.secure_url === oldUser.cv.secure_url) || (newProfile.cv.url === oldUser.cv.url))) newProfile.cv = oldUser.cv;

      else if (newProfile.cv) {

        await this.mediaService.uploadFile('user', newProfile.email, newProfile.cv)
          .then((file: MediaFile) => newProfile.cv = file)
          .catch((error) => this.utilsService.openErrorAlert(error.error));
      }

      this.updateProfile(newProfile);
    });
  }

  public editAvatar($event, profile) {

    const vm = this;
    const fileReader = new FileReader();

    fileReader.onload = function () {
      profile.avatar.url = fileReader.result.toString();
      vm.uploadAvatar(profile);
    }

    fileReader.readAsDataURL($event.target.files[0]);
  }

  private uploadAvatar(profile: User) {

    this.mediaService.uploadFile('user', profile.email, profile.avatar.url)
      .then((file: MediaFile) => {
        profile.avatar = file;
        this.updateProfile(profile);
      })
      .catch((error) => this.utilsService.openErrorAlert(error.error));
  }

  private updateProfile(profile: User) {

    this.profileService.updateProfile(profile).subscribe(
      (success) => {
        sessionStorage.setItem('email', profile.email);
        this.utilsService.showInfoMsg('ecamp-info-success', 'SERVER.MESSAGES.USER.UPDATED')
      },
      (error) => this.utilsService.openErrorAlert(error.error)
    );
  }

  public goToBills() {
    this.router.navigate(['ecamp/crm/bills']);
  }

  public goToCamps(type: number) {
    this.router.navigate(['ecamp/crm/camps'], { queryParams: { type } });
  }

  public goToParticipants() {
    this.router.navigate(['ecamp/crm/participants']);
  }

  public goToProgrammings() {
    this.router.navigate(['ecamp/crm/programmings']);
  }

  public goToCharts() {
    this.router.navigate(['ecamp/crm/charts']);
  }

  public goToUsers(role: number) {
    this.router.navigate(['ecamp/crm/users'], { queryParams: { role } });
  }
}