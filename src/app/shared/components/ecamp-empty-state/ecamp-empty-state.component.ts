import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ecamp-empty-state',
  templateUrl: './ecamp-empty-state.component.html',
  styleUrls: ['./ecamp-empty-state.component.scss']
})
export class EcampEmptyStateComponent implements OnInit {

  @Input() message: string;

  constructor() { }

  ngOnInit() { }
}