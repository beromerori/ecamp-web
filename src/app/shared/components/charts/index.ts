/** ·········· Charts  ·········· */

// Bar
import { EcampBarChartComponent } from './ecamp-bar-chart/ecamp-bar-chart.component';
export { EcampBarChartComponent } from './ecamp-bar-chart/ecamp-bar-chart.component';

// Pie
import { EcampPieChartComponent } from './ecamp-pie-chart/ecamp-pie-chart.component';
export { EcampPieChartComponent } from './ecamp-pie-chart/ecamp-pie-chart.component';

export const Charts: any[] = [
    EcampBarChartComponent,
    EcampPieChartComponent
];