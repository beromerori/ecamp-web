import { Component, Input, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

// Chart
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';

// Models
import { Chart } from '../../../models/chart';

@Component({
  selector: 'app-ecamp-pie-chart',
  templateUrl: './ecamp-pie-chart.component.html',
  styleUrls: ['./ecamp-pie-chart.component.scss']
})
export class EcampPieChartComponent implements OnInit {

  /*@Input() labels: Label[];*/
  @Input() chart: Chart;
  @Input() data: number[];
  @Input() lang: string;
  @Input() colors: Array<{ backgroundColor: string[] }> = [
    {
      backgroundColor: ['#49cc90', '#fca130'],
    }
  ];

  public labels: Label[];

  public type: ChartType = 'pie';

  public options: ChartOptions = {
    responsive: true,
    legend: { position: 'top' }
  };

  constructor(private translateService: TranslateService) { }

  ngOnInit() { }

  ngOnChanges() {
    this.labels = this.chart.totalsAndByType.campsByType.labels.map(label => this.translateService.instant(label));
  }
}