import { Component, Input, OnInit, OnChanges } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

// Chart
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

import { CONST } from '../../../../utils/constants';

// Models
import { Chart } from '../../../models/chart';

@Component({
  selector: 'app-ecamp-bar-chart',
  templateUrl: './ecamp-bar-chart.component.html',
  styleUrls: ['./ecamp-bar-chart.component.scss']
})
export class EcampBarChartComponent implements OnInit, OnChanges {

  @Input() chart: Chart;
  @Input() lang: string;

  public labels: Label[] = [];
  public data: ChartDataSets[] = [{ data: [], backgroundColor: [], hoverBackgroundColor: [], hoverBorderColor: [] }];

  public type: ChartType = 'bar';

  public options: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 10,
          stepSize: 1
        }
      }]
    },
    legend: {
      display: false
    },
  };

  constructor(private translateService: TranslateService) { }

  ngOnInit() {

    CONST.MODALITIES.forEach((modality, index) => {
      this.labels.push(this.translateService.instant(modality.name));
      this.data[0].backgroundColor[index] = modality.color;
      this.data[0].hoverBackgroundColor[index] = modality.color;
      this.data[0].hoverBorderColor[index] = modality.color;
    });

    this.data[0].data = this.chart.byModalities.data;

    this.options.scales.yAxes[0].ticks.max = this.chart.totalsAndByType.totalCamps;
  }

  ngOnChanges() {
    //this.labels = this.chart.byModalities.labels.map(label => this.translateService.instant(label));
  }
}