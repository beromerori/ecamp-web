import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

// Modal
import { overlayConfigFactory, Modal } from 'ngx-modialog';

// My components (barrel)
import { CampModalComponent } from '../../../shared/components/modals';

// My models
import { Camp, Modality } from '../../models';

// My Services (barrel)
import { AuthService } from '../../services';
import { UtilsService } from '../../../utils/utils.service';

@Component({
  selector: 'app-small-camp',
  templateUrl: './small-camp.component.html',
  styleUrls: ['./small-camp.component.scss']
})
export class SmallCampComponent implements OnInit {

  @Input() camp: Camp;
  @Input() index: number;
  @Input() isPrivate: boolean;

  @Output() update: EventEmitter<Camp> = new EventEmitter<Camp>();
  @Output() delete: EventEmitter<Camp> = new EventEmitter<Camp>();

  public userLogged$ = this.authService.userLogged$;

  public checkedModalities: Modality[] = [];

  constructor(
    private modal: Modal,
    private utilsService: UtilsService,
    private authService: AuthService) { }

  ngOnInit() {
    this.checkedModalities = this.camp.modalities.filter(modality => modality.checked);
  }

  public editCamp() {

    const oldCamp: Camp = JSON.parse(JSON.stringify(this.camp));

    this.camp.modalities = this.utilsService.getModalities(this.camp.modalities);

    const editCampModal = this.modal.open(CampModalComponent, overlayConfigFactory({
      dialogClass: 'ecamp-modal-lg ecamp-modal',
      camp: this.camp
    }));

    editCampModal.result.then((updatedCamp: Camp) => {

      if (!updatedCamp) {
        this.camp = oldCamp;
        return;
      }

      this.update.emit(updatedCamp);
    });
  }

  public removeCamp() {

    const alert = this.utilsService.openAlert(1, 'ALERT.REMOVE-CAMP-ANSWER');

    alert.afterClosed().subscribe((deleted: boolean) => {
      if (deleted) this.deleteCamp();
    });
  }

  private deleteCamp() {
    this.delete.emit(this.camp);
  }
}