import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-media-manager',
  templateUrl: './media-manager.component.html',
  styleUrls: ['./media-manager.component.scss']
})
export class MediaManagerComponent implements OnInit {

  @Input() code: string;
  @Input() images = [];

  @Output() updateImages: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() { }

  public isFull() {
    return this.images.length === 8;
  }

  public addImage($event) {

    const vm = this;
    const fileReader = new FileReader();

    fileReader.onload = function () {
      vm.images.push({ index: vm.images.length, url: fileReader.result.toString(), alt: '' });
      vm.updateImages.emit(vm.images);
    }

    fileReader.readAsDataURL($event.target.files[0]);
  }

  public deleteImage(index: number) {
    this.images.splice(index, 1);
    this.updateImages.emit(this.images);
  }
}