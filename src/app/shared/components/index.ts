// Alert
import { AlertDialogComponent } from './alert/alert-dialog/alert-dialog.component';
export { AlertDialogComponent } from './alert/alert-dialog/alert-dialog.component';

import { AlertPaypalComponent } from './alert/alert-paypal/alert-paypal.component';
export { AlertPaypalComponent } from './alert/alert-paypal/alert-paypal.component';

// Navigation
import { CrmNavigationComponent } from './crm-navigation/crm-navigation.component';
export { CrmNavigationComponent } from './crm-navigation/crm-navigation.component';

// Section title
import { CrmSectionTitleComponent } from './crm-section-title/crm-section-title.component';
export { CrmSectionTitleComponent } from './crm-section-title/crm-section-title.component';

// Carousel
import { EcampCarouselComponent } from './ecamp-carousel/ecamp-carousel.component';
export { EcampCarouselComponent } from './ecamp-carousel/ecamp-carousel.component';

// Empty-state
import { EcampEmptyStateComponent } from './ecamp-empty-state/ecamp-empty-state.component';
export { EcampEmptyStateComponent } from './ecamp-empty-state/ecamp-empty-state.component';

// Footer
import { EcampFooterComponent } from './ecamp-footer/ecamp-footer.component';
export { EcampFooterComponent } from './ecamp-footer/ecamp-footer.component';

// Info message
import { EcampInfoMessageComponent } from './ecamp-info-message/ecamp-info-message.component';
export { EcampInfoMessageComponent } from './ecamp-info-message/ecamp-info-message.component';

// Input error
import { EcampInputErrorComponent } from './ecamp-input-error/ecamp-input-error.component';
export { EcampInputErrorComponent } from './ecamp-input-error/ecamp-input-error.component';

// Loading
import { EcampLoadingComponent } from './ecamp-loading/ecamp-loading.component';
export { EcampLoadingComponent } from './ecamp-loading/ecamp-loading.component';

// Pagination
import { EcampPaginationComponent } from './ecamp-pagination/ecamp-pagination.component';
export { EcampPaginationComponent } from './ecamp-pagination/ecamp-pagination.component';

// Toolbar
import { EcampToolbarComponent } from './ecamp-toolbar/ecamp-toolbar.component';
export { EcampToolbarComponent } from './ecamp-toolbar/ecamp-toolbar.component';

// Login form
import { LoginFormComponent } from './login-form/login-form.component';
export { LoginFormComponent } from './login-form/login-form.component';

// Media manager
import { MediaManagerComponent } from './media-manager/media-manager.component';
export { MediaManagerComponent } from './media-manager/media-manager.component';

// Modality badge
import { ModalityBadgeComponent } from './modality-badge/modality-badge.component';
export { ModalityBadgeComponent } from './modality-badge/modality-badge.component';

// Small camp
import { SmallCampComponent } from './small-camp/small-camp.component';
export { SmallCampComponent } from './small-camp/small-camp.component';

/** ·········· Charts  ·········· */
import { Charts } from './charts';
export { Charts } from './charts';

/** ·········· Modals  ·········· */
import { Modals } from './modals';
export { Modals } from './modals';

/** ·········· Tables  ·········· */
import { Tables } from './tables';
export { Tables } from './tables';

export const Components: any[] = [
    AlertDialogComponent,
    AlertPaypalComponent,
    CrmNavigationComponent,
    CrmSectionTitleComponent,
    EcampCarouselComponent,
    EcampEmptyStateComponent,
    EcampFooterComponent,
    EcampInfoMessageComponent,
    EcampInputErrorComponent,
    EcampLoadingComponent,
    EcampPaginationComponent,
    EcampToolbarComponent,
    LoginFormComponent,
    MediaManagerComponent,
    ModalityBadgeComponent,
    SmallCampComponent,
    // Charts
    ...Charts,
    // Modals
    ...Modals,
    // Tables
    ...Tables
];