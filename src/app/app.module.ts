import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// Animations
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Loading
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

// Redux
import { RootReducerModule } from './redux/reducers/root-reducer.module';

// Routing
import { AppRouting } from './app-routing.module';
import { AppComponent } from './app.component';

// My guards
import { Guards } from './shared/services/guards';

// My interceptors
import { RequestInterceptor } from './shared/services/interceptors/request.interceptor';
import { TokenInterceptor } from './shared/services/interceptors/token.interceptor';

// My modules
import { SharedModule } from './shared/shared.module';

// My pages
import { CrmLoginComponent } from './pages/crm/crm-login/crm-login.component';

// My services
import { Services } from './shared/services';
import { BaseEntityService } from './utils/base-entity.service';
import { LoadingService } from './utils/loading.service';
import { OverlaysService } from './utils/overlays.service';
import { UtilsService } from './utils/utils.service';

@NgModule({
  declarations: [
    AppComponent,
    CrmLoginComponent
  ],
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    RootReducerModule,
    AppRouting,
    SharedModule,
    MatProgressSpinnerModule
  ],
  exports: [],
  providers: [
    BaseEntityService,
    LoadingService,
    OverlaysService,
    UtilsService,
    // Guards
    ...Guards,
    // Interceptors
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    // Services
    ...Services
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }